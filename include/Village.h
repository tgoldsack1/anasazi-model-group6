#ifndef VILLAGE
#define VILLAGE

#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedDiscreteSpace.h"
#include "Location.h"
#include "Household.h"
#include <vector>

/* compiler not recognising Location and household, thus I am declaring them here */
class Location;
class Household;

class Village{
private:
	repast::AgentId villageId;
	Location* centre;
	std::vector<Household*> households;

public:
	Village( repast::AgentId id, Location* centre, std::vector<Household*> households);
	~Village();

	/* Required Getters */
	virtual repast::AgentId& getId() { return villageId; }
	virtual const repast::AgentId& getId() const { return villageId; }

	/* Methods specific to this class */
	int getCurrentCapacity();

	std::vector<Household*> getHouseholds() { return households; }
	void addHousehold( Household* household ); // unlike the UML (check with T & B)
	void removeHousehold( repast::AgentId householdId );
	void redistributeMaize( int needs );

};

#endif
