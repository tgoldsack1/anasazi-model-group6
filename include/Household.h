#ifndef HOUSEHOLD
#define HOUSEHOLD

#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedDiscreteSpace.h"
#include "repast_hpc/Random.h"
#include "Location.h"
#include "Village.h" 	// implementing proposed improvements:

/* compiler not recognising Location and Village, thus I am declaring them here */
class Location;
class Village;

class Household{
private:
	repast::AgentId householdId;
	Location* assignedField;
	Village* assignedVillage; 	// implementing proposed improvements:
	int maizeStorage;
	int age;
	int deathAge;

public:
	Household(repast::AgentId id,int a, int deathAge, int mStorage);
	~Household();

	/* Required Getters */
	virtual repast::AgentId& getId() { return householdId; }
	virtual const repast::AgentId& getId() const { return householdId; }

	/* Getters specific to this kind of Agent */
	Location* getAssignedField(){return assignedField; }
	int splitMaizeStored(int percentage);
	bool checkMaize(int needs);
	bool death();
	int getDeathAge();
	int getAge();
	bool fission(int minFissionAge, int maxFissionAge, double gen, double fProb);
	void nextYear(int needs);
	void chooseField(Location* Field);

	// implementing proposed improvements:
	void setAssignedVillage(Village* village);
	void setMaizeStorage(int sharedStoredMaize); //unlike UML (UML suggests the function should return int!). Check with team!
	int getMaizeStorage(); //in UML but was not implemented
	Village* getAssignedVillage();

};

#endif
