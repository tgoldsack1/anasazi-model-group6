#library(density)
library(here)

target_data <- read.csv(here("data", "target_data.csv"), header = FALSE)
target_years <- target_data[[1]]
target_numHouseholds <- target_data[[2]]
t_target_nums = t(target_data$V2)

calibratedData1 = readRDS("acse6312_group_6_test_calibration", refhook = NULL)
calibratedData2 = readRDS("acse6312_group_6_test_calibration_ 2204", refhook = NULL)
calibratedData3 = readRDS("acse6312_group_6_test_calibration_ 29720", refhook = NULL)
calibratedData4 = readRDS("acse6312_group_6_test_calibration_ 315416", refhook = NULL)
calibratedData5 = readRDS("acse6312_group_6_test_calibration_ 325784", refhook = NULL)
calibratedData6 = readRDS("acse6312_group_6_test_calibration_ 749828", refhook = NULL)
calibratedData7 = readRDS("acse6312_group_6_test_calibration_ 825222", refhook = NULL)
calibratedData8 = readRDS("acse6312_group_6_test_calibration_ 986945", refhook = NULL)
calibratedData9 = readRDS("acse6312_group_6_test_calibration_99", refhook = NULL)
calibratedData10 = readRDS("acse6312_group_6_test_calibration_ 17284", refhook = NULL)
calibratedData11 = readRDS("acse6312_group_6_test_calibration_ 318924", refhook = NULL)
calibratedData12 = readRDS("acse6312_group_6_test_calibration_ 333179", refhook = NULL)
calibratedData13 = readRDS("acse6312_group_6_test_calibration_ 419769", refhook = NULL)
calibratedData14 = readRDS("acse6312_group_6_test_calibration_ 544283", refhook = NULL)
calibratedData15 = readRDS("acse6312_group_6_test_calibration_ 812603", refhook = NULL)
calibratedData16 = readRDS("acse6312_group_6_test_calibration_ 938149", refhook = NULL)

stats1 = calibratedData5['stats']

print(class(stats1[[1]]))
print(dim(stats1[[1]])[1])
print(stats1[[1]][4,])

cds = list(calibratedData1, calibratedData2, calibratedData3, calibratedData4, calibratedData5,
           calibratedData6, calibratedData7, calibratedData8, calibratedData9, calibratedData10,
           calibratedData11, calibratedData12, calibratedData13, calibratedData14, calibratedData15,
           calibratedData16)


all_params <- Map(function(x) x['param'], cds)
all_params1 <- Reduce(function(x, y) rbind(x, y), all_params)
all_params2 <- Reduce(function(x, y) rbind(x, y), all_params1)

all_stats <- Map(function(x) x['stats'], cds)
all_stats1 <- Reduce(function(x, y) rbind(x, y), all_stats)
all_stats2 <- Reduce(function(x, y) rbind(x, y), all_stats1)

all_nsims <- Map(function(x) x['nsim'], cds)
all_nsims1 <- Reduce(function(x, y) rbind(x, y), all_nsims)
all_nsims2 <- Reduce(function(x, y) x + y, all_nsims1)

all_nrec <- Map(function(x) x['nrec'], cds)
all_nrec1 <- Reduce(function(x, y) rbind(x, y), all_nrec)
all_nrec2 <- Reduce(function(x, y) x + y, all_nrec1)

dists = list()
for (i in 1:dim(all_stats2)[1]) {
  dist = dist(rbind(all_stats2[i, ], t_target_nums))
  dists[i] <- dist
}

dists1 <- Reduce(function(x, y) rbind(x, y), dists)

indexes = 1:70
indexes = as.data.frame(indexes)
my_data = cbind(cbind(cbind(indexes, dists1), all_params2), all_stats2)
sorted_idx = order(my_data$dists1)
my_data = my_data[sorted_idx, ]


remove_rows = list()
unique_data = list()
for (i in 1:dim(my_data)[1]) {
  unique = TRUE
  data = my_data[i, 13:563]
  for (u_data in unique_data) {
    if (identical(as.numeric(data), u_data)) {
      unique = FALSE
    }
  }
  if (unique) {
    unique_data[[length(unique_data)+1]] <- as.numeric(data)
  } else {
    remove_rows[[length(remove_rows)+1]] <- my_data[i, 1]
  }
}

removed_dups <- my_data[-c(unlist(remove_rows)), ]

for (i in 1:dim(removed_dups)[1]) {
  plot_data = removed_dups[i, 13:563]
  plot(target_years, target_numHouseholds, type="l", col="red", xlab = "years", ylab="households")
  lines(target_years, plot_data, type="l")
}



saveRDS(removed_dups, file = "all_calibrations")
saveRDS(removed_dups[1:10, ], file = "top_10")
