#include <boost/mpi.hpp>
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedDiscreteSpace.h"
#include "repast_hpc/GridComponents.h"
#include "repast_hpc/Moore2DGridQuery.h"

#include "Model.h"
#include "Location.h"
#include "Household.h"
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <ctime>

// Robin: during testing people typically run large parts of the model with a
// reduced population (and timespan). It also may not be possible to run all tests as black block
// unit tests (some parts lack getter and setter functions) so these may need
// to be added to the current implementation.

// Print out should be in the format:
// Expected Value: ...
// Returned Value: ...
// Test passed: True/False

/*
 Instantiate an agent
*/
bool testInstantiateAgent(){
  printf("\n\n---- Begin Test 1: Instantiate Agent unit test ----\n\n");


  int test_num [7];           // creates array with 7 random numbers for testing between 0-9 with each element as follows
  for (int i=0;i<7;i++){          // test_num = [id, startProc, agentType, currentProc, age, death age, maize storage]
    test_num[i] = rand()%30;
  }
  repast::AgentId id(test_num[0], test_num[1], test_num[2], test_num[3]); // Creates agent ID with first 4 random numbers
  Household* household = new Household(id,test_num[4],test_num[5],test_num[6]); //  creates object household
  if ((household->getId() == id) && (household->getAge() == test_num[4])&&(household->getDeathAge() == test_num[5])&&(household->getMaizeStorage() == test_num[6])){
    std::cout << "\nExpected Agent ID: " << id <<std::endl;
    std::cout << "Returned Agent ID: " << household->getId() <<std::endl;
    std::cout << "Expected Agent age: " << test_num[4]<<std::endl;
    std::cout << "Returned Agent age: " << household->getAge() <<std::endl;
    std::cout << "Expected Agent death age: " << test_num[5] <<std::endl;
    std::cout << "Returned Agent death age: " << household->getDeathAge() <<std::endl;
    std::cout << "Expected Agent Maize storage: " << test_num[6] <<std::endl;
    std::cout << "Returned Agent Maize storage: " << household->getMaizeStorage() <<std::endl;
    std::cout << "Agent instantiated successfully" << std::endl << "Test passed\n"<<std::endl;
    delete household;

    printf("\n\n---- End Instantiate Agent unit test ----\n\n");
    return true;
  } else{
    std::cout << "\nExpected Agent ID: " << id <<std::endl;
    std::cout << "Returned Agent ID: " << household->getId() <<std::endl;
    std::cout << "Expected Agent age: " << test_num[4]<<std::endl;
    std::cout << "Returned Agent age: " << household->getAge() <<std::endl;
    std::cout << "Expected Agent death age: " << test_num[5] <<std::endl;
    std::cout << "Returned Agent death age: " << household->getDeathAge() <<std::endl;
    std::cout << "Expected Agent Maize storage: " << test_num[6] <<std::endl;
    std::cout << "Returned Agent Maize storage: " << household->getMaizeStorage() <<std::endl;
    std::cout << "Agent instantiated unsuccessfully" << std::endl << "Test failed\n"<<std::endl;

    printf("\n\n---- End Instantiate Agent unit test ----\n\n");
    return false;
  }
}

/*
Update the environment with new
archaeological data
*/
bool testUpdateEnv(int argc, char** argv){   // must be run with the new data in the directory labelled as x_new

  printf("\n\n---- Begin Test 2: Update Environment unit test ----\n\n");


  std::string configFile = argv[1]; // The name of the configuration file
	std::string propsFile  = argv[2]; // The name of the properties file
	boost::mpi::environment env(argc, argv);
	boost::mpi::communicator* world;

	repast::RepastProcess::init(configFile);
	world = new boost::mpi::communicator;
	AnasaziModel* model = new AnasaziModel(propsFile, argc, argv, world);
	repast::ScheduleRunner& runner = repast::RepastProcess::instance()->getScheduleRunner();

  model->initAgents();

  char oldName_map[] = "data/map_new.csv";    // Selects the new files to be renamed
  char oldName_water[] = "data/water_new.csv";
  char oldName_pdsi[] = "data/pdsi_new.csv";
  char oldName_hydro[] = "data/hydro_new.csv";

  char tempName_map[] = "data/map_temp.csv";    // Sets the name of the new files
  char tempName_water[] = "data/water_temp.csv";
  char tempName_pdsi[] = "data/pdsi_temp.csv";
  char tempName_hydro[] = "data/hydro_temp.csv";

  char newName_map[] = "data/map.csv";    // Sets the name of the new files
  char newName_water[] = "data/water.csv";
  char newName_pdsi[] = "data/pdsi.csv";
  char newName_hydro[] = "data/hydro.csv";


  printf("\nConfirm first line of old pdsi data file in intitial model:\n\n");

  std::ifstream pdsi_file ("data/pdsi.csv");

  int i=0; string temp1;
  pdsi_file.ignore(500,'\n');         //Ignore first line 
  getline(pdsi_file,temp1,',');

  string columnNames[] = {"year","general","north","mid","natural","upland","kinbiko"};

  double targetValues[] = {
    model->getPdsiYear(i), 
    model->getPdsiGeneral(i),
    model->getPdsiNorth(i),
    model->getPdsiMid(i),
    model->getPdsiNatural(i),
    model->getPdsiUpland(i),
    model->getPdsiKinbiko(i) 
  };

  int sizeOfTargets = sizeof(targetValues)/sizeof(targetValues[0]);  

  bool testPassed = true;

  for (int i=0; i<sizeOfTargets; i++){        // compares the data stored in modelUp with the new Pdsi file (first line)
    std::cout<<"Expected"<<" ";
    std::cout<<columnNames[i]<<": ";
    std::cout<<repast::strToDouble(temp1)<<"\n";
    std::cout<<"Returned"<<" ";
    std::cout<<columnNames[i]<<": ";
    std::cout<<targetValues[i]<<"\n\n";  
    if (targetValues[i] != repast::strToDouble(temp1)){
      testPassed = false;
    }
    getline(pdsi_file,temp1,',');
  }

  if (!testPassed) {
    std::cout<<"Initial data was incorrect"<<std::endl ;
    std::cout<<"Test 2 failed"<<std::endl ;
    return false;
  }

  std::ifstream map_fileN ("data/map_new.csv");   //sets up new data files to be read from
  std::ifstream water_fileN ("data/water_new.csv");
  std::ifstream pdsi_fileN ("data/pdsi_new.csv");
  std::ifstream hydro_fileN ("data/hydro_new.csv");

  int resultMap, resultWater, resultPdsi, resultHydro;

  if(map_fileN.is_open()){    //checks to see if new data files are present
    std::cout<<"New map data detected"<<std::endl;

    if(water_fileN.is_open()){
      std::cout<<"New water data detected"<<std::endl;

      if(pdsi_fileN.is_open()){
        std::cout<<"New pdsi data detected"<<std::endl;

        if(hydro_fileN.is_open()){

          std::cout<<"New map data detected"<<std::endl;

          // Give current data a temporary name so it doesn't get overwritten
          resultMap = std::rename(newName_map, tempName_map);      // renaming process (returns 0 if name relabel successful)
          resultWater = std::rename(newName_water, tempName_water);
          resultPdsi = std::rename(newName_pdsi, tempName_pdsi);
          resultHydro = std::rename(newName_hydro, tempName_hydro);

          // Rename new data so it can be read in to model
          resultMap = std::rename(oldName_map, newName_map);      // renaming process (returns 0 if name relabel successful)
          resultWater = std::rename(oldName_water, newName_water);
          resultPdsi = std::rename(oldName_pdsi, newName_pdsi);
          resultHydro = std::rename(oldName_hydro, newName_hydro);

        } else { 
          std::cout<<"New hydro data not detected (please move data into anasazi-model-group6/data labelled as hydro_new.csv)"<<std::endl;
          std::cout<<"Test failed"<<std::endl;
          printf("\n\n---- End Update Environment unit test ----\n\n");
          return false;
        }
      } else { 
        std::cout<<"New pdsi data not detected (please move data into anasazi-model-group6/data labelled as pdsi_new.csv)"<<std::endl;
        std::cout<<"Test failed"<<std::endl;
        printf("\n\n---- End Update Environment unit test ----\n\n");
        return false;
      }
    } else { 
      std::cout<<"New water data not detected (please move data into anasazi-model-group6/data labelled as water_new.csv)"<<std::endl;
      std::cout<<"Test failed"<<std::endl; 
      printf("\n\n---- End Update Environment unit test ----\n\n");
      return false;
    }
  } else { 
    std::cout<<"New map data not detected (please move data into anasazi-model-group6/data labelled as map_new.csv)"<<std::endl;
    std::cout<<"Test failed"<<std::endl;
    printf("\n\n---- End Update Environment unit test ----\n\n");
    return false;
  }

  delete model;
  model = new AnasaziModel(propsFile, argc, argv, world);   // creates new model to update with new data
  model->initAgents();    //Reads in new data to this model

  i=0; 
  temp1="";
  pdsi_fileN.ignore(500,'\n');         //Ignore first line 
  getline(pdsi_fileN,temp1,',');

  printf("\nConfirm first line of new pdsi data file in new model:\n\n");

  double newTargetValues[] = {
    model->getPdsiYear(i), 
    model->getPdsiGeneral(i),
    model->getPdsiNorth(i),
    model->getPdsiMid(i),
    model->getPdsiNatural(i),
    model->getPdsiUpland(i),
    model->getPdsiKinbiko(i) 
  };

  sizeOfTargets = sizeof(newTargetValues)/sizeof(newTargetValues[0]);  

  testPassed = true;

  for (int i=0; i<sizeOfTargets; i++){        // compares the data stored in modelUp with the new Pdsi file (first line)
    std::cout<<"Expected"<<" ";
    std::cout<<columnNames[i]<<": ";
    std::cout<<repast::strToDouble(temp1)<<"\n";
    std::cout<<"Returned"<<" ";
    std::cout<<columnNames[i]<<": ";
    std::cout<<newTargetValues[i]<<"\n\n";  
    if (newTargetValues[i] != repast::strToDouble(temp1)){
      testPassed = false;
    }
    getline(pdsi_fileN,temp1,',');
  } 

  // Revert files back to their original names
  resultMap = std::rename(newName_map, oldName_map);      // renaming process (returns 0 if name relabel successful)
  resultWater = std::rename(newName_water, oldName_water);
  resultPdsi = std::rename(newName_pdsi, oldName_pdsi);
  resultHydro = std::rename(newName_hydro, oldName_hydro);

  resultMap = std::rename(tempName_map, newName_map);      // renaming process (returns 0 if name relabel successful)
  resultWater = std::rename(tempName_water, newName_water);
  resultPdsi = std::rename(tempName_pdsi, newName_pdsi);
  resultHydro = std::rename(tempName_hydro, newName_hydro);

  if (testPassed) {
    std::cout<<"New data updated sucessfully"<<std::endl ;
    std::cout<<"Test 2 passed"<<std::endl ;
  } else {
    std::cout<<"Test 2 failed"<<std::endl ;
  }
  
  map_fileN.close();
  water_fileN.close();
  pdsi_fileN.close();
  hydro_fileN.close();

  printf("\n\n---- End Update Environment unit test ----\n\n");

  return testPassed;

}
/*
Read a new ‘death age’ parameter
at run-time and demonstrate the
intended effect on agent life expectancy
*/
bool testReadDeathAge(int argc, char** argv, 	boost::mpi::communicator* comm){
  // I think this happens in Model.cpp line 96

  printf("\n\n---- Begin Test 3: Read Death Age unit test ----\n\n");

  std::string props1 = "tests/test_props/test3a.props";
  std::string props2 = "tests/test_props/test3b.props";

  // Initialise model
  AnasaziModel* model = new AnasaziModel(props1, argc, argv, comm);

  printf("Initial parameters read: death age 30-35 \n\n");

  model->initAgents();

  std::vector<Household*> agents = model->getAgents();
  bool allInBounds1 = true;

  printf("Population death ages (pre-new parameter): \n");
  for (int i = 0; i < agents.size(); i++) {
    int da = agents[i]->getDeathAge();
    std::cout << da << ' ';
    if (da < 30 or da > 35 ){
      allInBounds1 = false;
    }
  }
  printf("\n\n");

  std::cout<<"30 <= Life expectancy <= 35:"<<"\n";
  std::cout<<"Expected: true"<<"\n";
  std::cout<<"Returned:"<<" ";
  std::cout << std::boolalpha;
  std::cout<<allInBounds1<<"\n\n";

  model = new AnasaziModel(props2, argc, argv, comm);
  printf("New parameters read: death age 20-25 \n\n");

  model->initAgents();

  agents = model->getAgents();
  bool allInBounds2 = true;

  printf("Population death ages (post-new parameter): \n");
  for (int i = 0; i < agents.size(); i++) {
    int da = agents[i]->getDeathAge();
    std::cout << da << ' ';
    if (da < 20 or da > 25 ){
      allInBounds2 = false;
    }
  }
  printf("\n\n");

  std::cout<<"20 <= Life expectancy <= 25:"<<"\n";
  std::cout<<"Expected: true"<<"\n";
  std::cout<<"Returned:"<<" ";
  std::cout << std::boolalpha;
  std::cout<<allInBounds2<<"\n";

  bool allPassed = allInBounds1 && allInBounds2;

  if (allPassed) {
    printf("\n\n---- Read Death Age test passed successfully ----\n\n");
    printf("\n---- End of Read Death Age unit test ----\n\n");
    return true;

  } else {
    printf("\n\n  ---- Generate-output-file test failed ----  \n\n");
    printf("\n---- End of Read Death Age unit test ----\n\n");
    return false;
  }

  printf("\n---- End of Read Death Age unit test ----\n\n");

  return true;
}

/*
Generate an output file recording
the annual number of households over
time, which is accurate with respect to
the simulation
*/
bool testGenOutputFile(	std::string propsFile, int argc, char** argv, boost::mpi::communicator* world ){
  //declare the begining of the test:
  printf("\n\n---- Begin Test 4: Generate-Output-File unit test ----\n\n");

  //confirm that the file to be created does not exist:
  printf("Checking if \"new_NumberOfHousehold.csv\" (the name of the new output file) exists in the program's directory: \n");
  if (!boost::filesystem::exists("new_NumberOfHousehold.csv")) {
    printf("Returned: A file with the name \"new_NumberOfHousehold.csv\" does not exist in the scope (directory) of this program.\n\n");
  } else {
    printf("Returned: A file with the name \"new_NumberOfHousehold.csv\" does exist in the scope (directory) of this program.\n\n");
  }

  //create the output file:
  std::ofstream newResultFile;
  newResultFile.open("new_NumberOfHousehold.csv");
	newResultFile << "Year,Number-of-Households" << endl;

  //inistantiate a model:
  AnasaziModel* test_model = new AnasaziModel(propsFile, argc, argv, world);
  //inistantiate household agents:
  test_model->initAgents();
  //follow agents for 5 years:
  for (int i = 0; i < 5; i++) {
    newResultFile << test_model -> getCurrentYear() << "," << test_model-> getHouseholds() << endl;
    test_model -> doPerTick();
  }
  //close output file and delete the model to close it's built-in output file.
  newResultFile.close();
  delete test_model;

  //confirm that the file was created successfully:
  int errorCounter = 0;
  bool OutputFileCreated = false;
  if (boost::filesystem::exists("new_NumberOfHousehold.csv")) {
    OutputFileCreated = true;
  } else {
    errorCounter++;
  }

  printf("Checking if \"new_NumberOfHousehold.csv\" was created successfully: \n\n\tExpected: true. Returned: %s.\n\n",
  OutputFileCreated? "true" : "false");


  //confirm that value recorded by the new file are coorect, with reference to the models csv file:
  printf("Checking if the values stored in \"new_NumberOfHousehold.csv\", if any, were accurate with respect to those stored by the model in its \"NumberOfHousehold.csv\" file.\n\n");
  std::ifstream newResultsFile ("new_NumberOfHousehold.csv"); //read data from unit-testing csv file
  std::ifstream ResultFile ("NumberOfHousehold.csv"); //read data from built-in csv file

  newResultsFile.ignore(500,'\n');//Ignore first line
  ResultFile.ignore(500,'\n');//Ignore first line

  while(1)//read until end of file
  {
    string newCSV, oldCSV;
    int newInt, oldInt;
    getline(newResultsFile,newCSV, ','); // read until next comma and store into newCSV
    getline(ResultFile,oldCSV, ','); // read until next comma and store into oldCSV

    if((!newCSV.empty()) || (!oldCSV.empty())) {
      newInt = repast::strToInt(newCSV);
      oldInt = repast::strToInt(oldCSV);
      printf("\tExpected: %d. Returned: %d.\n", oldInt, newInt);
      if (!(oldInt == newInt)) {
        errorCounter++;
      }

      getline(newResultsFile, newCSV, '\n'); // read until next line and store into newCSV
      getline(ResultFile, oldCSV, '\n'); // read until next line and store into oldCSV
      newInt = repast::strToInt(newCSV);
      oldInt = repast::strToInt(oldCSV);
      printf("\tExpected: %d. Returned: %d.\n", oldInt, newInt);
      if (!(newCSV == oldCSV)) {
        errorCounter++;
      }

    }
    else{
      goto endloop;
    }
  }
  endloop: ;

  if (errorCounter == 0) {
    printf("\n\n  ---- Generate-output-file test passed successfully ----  \n\n");
    printf("\n---- End of Generate Output File unit test ----\n\n");
    return true;
  } else {
    printf("\n\n  ---- Generate-output-file test failed ----  \n\n");
    printf("\n---- End of Generate Output File unit test ----\n\n");
    return false;
  }
}

/*
Helper function which, given a vector
of Household agents, prints out all Agent IDs
and their Field Location IDs*/
void printAgentsAndFieldLocations(std::vector<Household*> agents,
  repast::SharedDiscreteSpace<Location, repast::StrictBorders, repast::SimpleAdder<Location> >* locationSpace,
  repast::SharedDiscreteSpace<Household, repast::StrictBorders, repast::SimpleAdder<Household> >* householdSpace) {

  printf("Household info: \n");
  for (int i = 0; i < agents.size(); i++) {
    repast::AgentId householdId = agents[i]->getId();
    Location* agentFieldLoc = agents[i]->getAssignedField();
    //repast::AgentId locationId = agentFieldLoc->getId();
    std::vector<int> resCoords;
    std::vector<int> fieldCoords;
    locationSpace->getLocation(agentFieldLoc->getId(), fieldCoords);
    householdSpace->getLocation(householdId, resCoords);

    printf("Household Id: ");
    std::cout << householdId.id() << " ";
    printf("| Residence location: ");
    for (std::vector<int>::const_iterator i = resCoords.begin(); i != resCoords.end(); ++i) {
      std::cout << *i << ' ';
    }
    printf("| Field location: ");
    for (std::vector<int>::const_iterator i = fieldCoords.begin(); i != fieldCoords.end(); ++i) {
      std::cout << *i << ' ';
    }
    // printf(", Field loc Id: ");
    // std::cout << locationId.id() << "";
    printf("| Field yield: ");
    std::cout << agentFieldLoc->getExpectedYield() << '\n';
  }
  printf("\n");
}

/*
Manipulate an agent’s field cell so
that it will not provide sufficient maize
for an agent. Check for the intended
behaviour of the agent*/
bool testManipulateAgentField(int argc, char** argv, 	boost::mpi::communicator* comm){
  // IF an agent derives not sufficient food from harvest and storage OR the age is beyond
  // maximum age of household THEN the agent is removed from the system

  // Agents who expect not to derive the required amount of food next year will move to a
  // new farm location and a plot to settle nearby.

  // The agent is not removed from the system at any point for this, so assume
  // the expected behavoir is to move

  printf("\n\n----- Begin Test 5: Manipulate Agent Field unit test ----\n\n");

  std::string props1 = "tests/test_props/test3a.props";

  // Initialise model
  printf("Initialise model. \n\n");
  AnasaziModel* model = new AnasaziModel(props1, argc, argv, comm);
  model->initAgents();

  // Get agents
  std::vector<Household*> agents = model->getAgents();

  repast::SharedDiscreteSpace<Location, repast::StrictBorders, repast::SimpleAdder<Location> >* locationSpace = model->getLocationSpace();
  repast::SharedDiscreteSpace<Household, repast::StrictBorders, repast::SimpleAdder<Household> >* householdSpace = model->getHouseholdSpace();

  // Print agents and field locations
  printAgentsAndFieldLocations(agents, locationSpace, householdSpace);

  // Get and print residence locations
  std::vector<Location*> locations = model->getLocations();
  std::vector<Location*> potentialMoveStates;

  // get possible move states
  for (int i = 0; i < locations.size(); i++) {
    if (locations[i]->getState() == 0) { // is not existing residence or field
      if (locations[i]->getExpectedYield() > 800) { // has sufficient yild
        potentialMoveStates.push_back(locations[i]);
      }
    }
  }

  // Get first agent (test subject) and store info
  Household* firstAgent = agents[0];
  repast::AgentId firstAgentId = firstAgent->getId();
  int firstAgentIdInt = firstAgentId.id();
  repast::AgentId firstAgentFieldId = firstAgent->getAssignedField()->getId();
  int firstAgentFieldIdInt = firstAgentFieldId.id();

  // Define variables regarding the correct move state
  int nearestMoveStateIdInt;
  std::vector<int>  nearestMoveStateCoords;
  int nearestMoveStateDistance;
  std::vector<int> distances;
  std::vector<int> resLoc;

  if(!potentialMoveStates.empty()){
    householdSpace->getLocation(firstAgentId, resLoc);

    for (int i = 0; i < potentialMoveStates.size(); i++) {
      std::vector<int> potLoc;
      locationSpace->getLocation(potentialMoveStates[i]->getId(),potLoc);
      distances.push_back(sqrt(pow((resLoc[0]-potLoc[0]),2) + pow((resLoc[1]-potLoc[1]),2)));
    }

    // get the smallest distance
    int minElementIndex = std::min_element(distances.begin(),distances.end()) - distances.begin();

    nearestMoveStateIdInt = (potentialMoveStates[minElementIndex]->getId()).id();
    locationSpace->getLocation(potentialMoveStates[minElementIndex]->getId(), nearestMoveStateCoords);
    nearestMoveStateDistance = distances[minElementIndex];
  }

  printf("Manipulate first agent's (id: ");
  std::cout<<firstAgentIdInt<< ") ";
  printf("field cell so yield is 0. \n\n");

  // Manupulate the desired agents field yield (and maize storage)
  firstAgent -> getAssignedField() -> calculateYield(0, 0.0, 0);
  firstAgent -> setMaizeStorage(0);

  agents = model->getAgents();

  printAgentsAndFieldLocations(agents, locationSpace, householdSpace);

  printf("Update household properties (do per tick function for household agents). \n\n");

  // Advance to next year
  model->updateHouseholdProperties();

  // Agent should move to a new location - either inside the valley or leaving
  std::vector<Household*> agents2 = model->getAgents();

  printAgentsAndFieldLocations(agents, locationSpace, householdSpace);

  std::vector<Location*> locations2 = model->getLocations();
  std::vector<int> residenceIds;

  bool isAgentPresent = false;
  int agentIndex;

  // Check vector for agent with ID == 2 (has agent left the valley)
  for (int i = 0; i < agents2.size(); i++) {
    if ((agents2[i]->getId()).id() == firstAgentIdInt) {
      isAgentPresent = true;
      agentIndex = i;
    }
  }

  Location* newFieldLocation = agents2[agentIndex]->getAssignedField();

  bool allPassed;

  if (isAgentPresent) { // agent has not left valley, confirm new field location is correct
    printf("Agent's (id: ");
    std::cout<<firstAgentIdInt<< ") ";
    printf("is still present: \n");
    std::cout<<"Returned: "<<" ";
    std::cout << std::boolalpha;
    std::cout<<isAgentPresent<<"\n\n";

    printf("Agent has not left valley, confirm new field location meets specification. \n\n");

    // Check is not same location
    bool isLocationDifferent = false;
    if ((newFieldLocation->getId()).id() != firstAgentFieldIdInt) {
      isLocationDifferent = true;
    }

    printf("Agent (id: ");
    std::cout<<firstAgentIdInt<< ") ";
    printf("has moved location: \n");
    std::cout<<"Expected: true"<<"\n";
    std::cout<<"Returned: "<<" ";
    std::cout << std::boolalpha;
    std::cout<<isLocationDifferent<<"\n\n";

    // CHECK NEW LOCATION MEETS SPEC

    // check if Yield of new location is above 800kg
    bool isYieldSufficient = false;
    if (newFieldLocation->getExpectedYield() > 800) {
      isYieldSufficient = true;
    }

    printf("Agent (id: ");
    std::cout<<firstAgentIdInt<< ") ";
    printf("new location has yield > 800kg: \n");
    std::cout<<"Expected: true"<<"\n";
    std::cout<<"Returned: "<<" ";
    std::cout << std::boolalpha;
    std::cout<<isYieldSufficient<<"\n\n";

    bool isNotTaken = true;

    // field location must not be an existing field or residence
    for (int i = 0; i < agents2.size(); i++) {
      if (i != agentIndex) {
        if ((agents2[i]->getAssignedField()->getId()).id() == (newFieldLocation->getId()).id()) {
          isNotTaken = false;
        }
        for (int j = 0; j < residenceIds.size(); j++) {
          if (residenceIds[j] == (newFieldLocation->getId()).id()) {
            isNotTaken = false;
          }
        }
      }
    }

    printf("Agent (id: ");
    std::cout<<firstAgentIdInt<< ") ";
    printf("new location is not existing field or residence: \n");
    std::cout<<"Expected: true"<<"\n";
    std::cout<<"Returned: "<<" ";
    std::cout << std::boolalpha;
    std::cout<<isNotTaken<<"\n\n";

    // Check they moved to the correct move space (nearest potential state)
    std::vector <int> newFieldCoords;
    locationSpace->getLocation(newFieldLocation->getId(), newFieldCoords);

    double newFieldLocDistance = sqrt(pow((resLoc[0]-newFieldCoords[0]),2) + pow((resLoc[1]-newFieldCoords[1]),2));
    double nearestMoveStateDistance = sqrt(pow((resLoc[0]-nearestMoveStateCoords[0]),2) + pow((resLoc[1]-nearestMoveStateCoords[1]),2));
    bool isNearestMoveState = newFieldLocDistance == nearestMoveStateDistance;

    printf("Agent (id: ");
    std::cout<<firstAgentIdInt<< ") ";
    printf("new location is the closest suitable \ncell to the agent's current residence: \n");
    std::cout<<"Expected: true"<<"\n";
    std::cout<<"Returned: "<<" ";
    std::cout << std::boolalpha;
    std::cout<<isNearestMoveState<<"\n\n";

    // If this test fails, show nearest move state + distance
    if (!isNearestMoveState){
      printf("\nActual move state: ");
      for (std::vector<int>::const_iterator i = newFieldCoords.begin(); i != newFieldCoords.end(); ++i) {
        std::cout << *i << ' ';
      }
      printf("\ndistance: ");
      std::cout<<newFieldLocDistance<<"\n";
      printf("\nNearest move state: ");
      for (std::vector<int>::const_iterator i = nearestMoveStateCoords.begin(); i != nearestMoveStateCoords.end(); ++i) {
        std::cout << *i << ' ';
      }
      printf("\ndistance: ");
      std::cout<<nearestMoveStateDistance<<"\n\n";
    }


    allPassed = isAgentPresent && isLocationDifferent && isYieldSufficient && isNotTaken && isNearestMoveState;
  } else { // agent has left he valley, confirm it should have
    printf("Agent has left the valley, confirm there were no suitable field locations. \n\n");

    allPassed = potentialMoveStates.empty();

    printf("There were no acceptable field locations: \n");
    std::cout<<"Expected: true"<<"\n";
    std::cout<<"Returned: "<<" ";
    std::cout << std::boolalpha;
    std::cout<<allPassed<<"\n\n";
  }

  if (allPassed) {
    printf("\n\n---- Manipulate Agent Field test passed successfully ----\n\n");
    printf("\n---- End of Manipulate Agent Field unit test ----\n\n");
    return true;

  } else {
    printf("\n\n  ---- Manipulate Agent Field test failed ----  \n\n");
    printf("\n---- End of Manipulate Agent Field unit test ----\n\n");
    return false;
  }
}




/*
Trigger an agent to move its
residence. Confirm that the new location
complies with the rules for residence
selection.
*/
bool testConfirmAgentMoves( std::string propsFile, int argc, char** argv, boost::mpi::communicator* world ){
  //
  printf("\n\n---- Begin Test 6: Confirm-Agent-Moves unit test ----\n\n");
  bool debug = 0; //debuging mode prints more information

  //declare variables/vectors/objects:
  std::vector<int> intialResidencePoints; //container to save the residence loaction of the selected hosuehold
  std::vector<int> residence; //container to save the residence loaction of the selected hosuehold
  std::vector<int> field; //container to save the field loaction of the selected hosuehold
  std::vector<Location*> locationObjects; //container to save location agents
  std::vector<Location*> neighbouringFields; //fields to around current resisdence to choose from
  std::vector<Household*> householdList; //container to save location agents
  Location* expectedSuitableLocation; //stores the expected moving-to location
  std::vector<Location*> neighbouringLocations;
  std::vector<Location*> suitableLocations; //if location complies with rules A, B and C
  std::vector<Location*> suitableLocationsRelax_C; //if location complies with rules A and B, but relaxing C
  std::vector<Location*> suitableLocationsRelax_A_C; //if location complies with rules B, but relaxing A and C
  std::vector<Location*> finalSuitableLocations; //final list of suitable locations
  std::vector<Location*> waterSources; //regardless of whether it is a field
  std::vector<Location*> waterSourcesNotField; //only if it is not a field
  std::vector<double> waterDistances; //distances between suitable locations and water sources

  //inistantiate a model:
  AnasaziModel* test_model = new AnasaziModel(propsFile, argc, argv, world);

  //inistantiate agents:
  test_model -> initAgents();

  //pick a household from the context and save its residence and field IDs:
  repast::SharedContext<Household>::const_iterator agents_iter = test_model -> getBeginIterator(); //grab an iterator pointing to the object at the begining of the context
  repast::SharedContext<Household>::const_iterator agents_end = test_model -> getEndIterator(); //grab an iterator pointing to the object at the end of the context
  Household* household = (&**agents_iter); //pick the first one pointed to by the iterator (it)
  const repast::AgentId _id = household -> getId(); //get a const id of the selected agent

  test_model -> getHouseholdSpace() -> getLocation(household -> getId(), intialResidencePoints); //get location of initial residence
  test_model -> getLocationSpace() -> getLocation(household -> getAssignedField() -> getId(), field); //get location of initial field
  test_model -> getLocationSpace() -> getObjectsAt(repast::Point<int>(intialResidencePoints[0], intialResidencePoints[1]), locationObjects); //get objects at location

  int _currentFldHarvest = household -> getAssignedField() -> getExpectedHarvest(); //get expected harvest of field
  int _currentResHarvest = locationObjects[0] -> getExpectedHarvest(); //get expected harvest of residence location
  int _needs = test_model -> getHouseholdNeeds(); //get hosuehold annual needs
  int _maizeStorage = household -> getMaizeStorage(); //check household maize storage

  printf("Selected: Household (%d, %d, %d), which has (%d) maize stored and needs (%d), is resident at location (%d, %d) and it is field is at loaction (%d, %d) with yield %d.\n\n",
  _id.id(), _id.startingRank(), _id.currentRank(), _maizeStorage, _needs, intialResidencePoints[0], intialResidencePoints[1], field[0], field[1], _currentFldHarvest);

  //select a field that is more than 1Km away from initial residence.
  repast::Moore2DGridQuery<Location> moore2DQueryField(test_model -> getLocationSpace()); //declare a 2d query to iterate through surrounding locations
  int rangeToSearch = 10; //1000/100 or max.Distance/100 (100 meters per cell = 1Km per 10 cells)
  int z = 1; //loop handler

  FieldLocationSearch: //the code will loop back from here if an appropriate location is not found within (rangeToSearch * z) after z++.

    moore2DQueryField.query(intialResidencePoints, (rangeToSearch * z), false, neighbouringFields); //get the surronding locations and save them in neighbouringFields
    for (std::vector<Location*>::iterator it = neighbouringFields.begin() ; it != neighbouringFields.end(); ++it){ //loop through the identified locations
      Location* tempLoc = (&**it); //grab the location pointed to by it
      std::vector<int> tempPoints; //container to store temp location points
      std::vector<Household*> tempHouseholdList; //container to save households in location

      test_model -> getLocationSpace() -> getLocation((&**it) -> getId(), tempPoints); //save cooridnates to get distance
      test_model -> getHouseholdSpace() -> getObjectsAt(repast::Point<int>(tempPoints[0], tempPoints[1]), tempHouseholdList); //check if households are at cell
      double _tempDistance = sqrt(pow((tempPoints[0] - intialResidencePoints[0]), 2) + pow((tempPoints[1] - intialResidencePoints[1]), 2)); //estimate distance between initial (current) residence and temp location
      int _tempFldtHarvest = (&**it) -> getExpectedHarvest(); //get harvest of temp location
      int _tempState = tempLoc -> getState(); //get state of temp location

      if (debug) {
        printf("tempLoc: (%d, %d), with state (%d), has (%d) households, harvest (%d) and is at (%.3f) km from initial residence.\n",
        tempPoints[0], tempPoints[1], _tempState, tempHouseholdList.size(), _tempFldtHarvest, _tempDistance / 10);
      }

      if ((_tempDistance > 10) && (_tempState != 2) && (tempHouseholdList.size() == 0) && (_tempFldtHarvest > _currentFldHarvest)) {
        household -> chooseField(tempLoc); //if current location satisfied the requirements, assign it to the household

        if (debug) {
          printf("suitableLoc: (%d, %d), with state (%d), has (%d) households, harvest (%d) and is at (%.3f) km from initial residence.\n",
          tempPoints[0], tempPoints[1], _tempState, tempHouseholdList.size(), _tempFldtHarvest, _tempDistance / 10);
        }
        goto doneSelectingNewField; //jump outside the loop
      }
    }

    z++; //widen the range if no suitable field was found
    goto FieldLocationSearch; //check more cells if not able to find one at (rangeToSearch * z) wide range

  doneSelectingNewField: //if a suitable location was found and assigned to the household


  //confirm new field assignment:
  field.clear();
  test_model -> getLocationSpace() -> getLocation(household -> getAssignedField() -> getId(), field); //copy new field's coordinates to vector
  _currentFldHarvest = household -> getAssignedField() -> getExpectedHarvest(); //update to current field's harvest
  double _tempDistanceFromFld = sqrt(pow((field[0] - intialResidencePoints[0]), 2) + pow((field[1] - intialResidencePoints[1]), 2));

  printf("\nAgent (%d, %d, %d) has now moved field to location (%d, %d), which has a potential yield of (%d), and is at %.3f Km from initial residence location.\n",
  _id.id(), _id.startingRank(), _id.currentRank(), field[0], field[1], _currentFldHarvest, (_tempDistanceFromFld / 10));

  //invoke relocate household function (this is the function that is expected to find a new suitable residence that satisfies the conditions):
  test_model -> relocateHousehold(household);

  locationObjects.clear();
  householdList.clear();
  test_model -> getHouseholdSpace() -> getLocation(household -> getId(), residence); //store new residence location in residence vector
  test_model -> getLocationSpace() -> getLocation(household -> getAssignedField() -> getId(), field); //update these to make sure they did not change
  test_model -> getHouseholdSpace() -> getObjectsAt(repast::Point<int>(residence[0], residence[1]), householdList); //get number of households in new residence location
  test_model -> getLocationSpace() -> getObjectsAt(repast::Point<int>(residence[0], residence[1]), locationObjects); //get number of objects in new household location, allows access location class functions (state/harvest/etc)
  _currentFldHarvest = household -> getAssignedField() -> getExpectedHarvest(); //get field harvest
  _currentResHarvest = locationObjects[0] -> getExpectedHarvest(); //get residence location harvest
  double distanceFromInitial = sqrt(pow((intialResidencePoints[0] - field[0]), 2) + pow((intialResidencePoints[1] - field[1]), 2)); //distance between new Field and old residence
  double distanceFromNew = sqrt(pow((residence[0] - field[0]), 2) + pow((residence[1] - field[1]), 2)); //distance between new Field and new residence

  printf("\nAgent (%d, %d, %d) has now moved residence to location (%d, %d), which has a potential yield of (%d), while its field is at loaction (%d, %d), which has a potential yield of (%d).\n",
  _id.id(), _id.startingRank(), _id.currentRank(), residence[0], residence[1], _currentResHarvest, field[0], field[1], _currentFldHarvest);

  //confirm moving rules:
  bool notField = false, residenceLowerYield = false, withinOneKilometer = false, closestToWater = false;

  //rule_1: move residence if distance from field is more than 1km:
  if (distanceFromInitial >= 10) {
    if (distanceFromNew < 10) {
      withinOneKilometer = true;
    }
  }
  printf("\nThe location of the new field is now %.3f Km from the initial location of the household residence. \n\nRule_1:\tExpected: new residence, which is at [%d, %d], to be within 1km (less than or equal to 10 cells) of the new field at location [%d, %d]). Returned a new residence at distance equal to \t%.3f Km\taway from the location of the new field. \n\t  ---- Rule_1 was %s ---- \n\n",
  distanceFromInitial / 10, residence[0], residence[1], field[0], field[1], distanceFromNew / 10, withinOneKilometer ? "obeyed" : "not obeyed");

  //rule_2: do not move to a Field:
  if ((locationObjects[0] -> getState() != 2)) {
    notField = true;
  }
  printf("Rule_2:\tExpected: new residence not to be at a location where a field is (state not equal 2). Returned: the state of the new residence loaction is \t%d. \nThere is(are) %d household(s) at the current location.\n\t  ---- Rule_2 was %s ---- \n\n",
  locationObjects[0] -> getState(), householdList.size(), notField ? "obeyed" : "not obeyed");

  //rule_3: residence must not have higher yield than field:
  if (!(_currentResHarvest > _currentFldHarvest)) {
    residenceLowerYield = true;
  }
  printf("Rule_3:\tExpected: the yield of the new residence (%d) not higher than that of the field's (%d), Returned: (%d is %s %d).\n\t  ---- Rule_3 was %s ---- \n\n",
  _currentResHarvest, _currentFldHarvest, _currentResHarvest, residenceLowerYield ? "not more than" : "more than", _currentFldHarvest, residenceLowerYield ? "obeyed" : "not obeyed");

  //rule_4: choose the cell nearest to a water cell if several cells available:
	test_model -> getLocationSpace() -> getObjectsAt(repast::Point<int>(intialResidencePoints[0], intialResidencePoints[1]), neighbouringLocations);
	Location* householdLocation = neighbouringLocations[0];

  if (debug) { //check correct location assignment and set state
    std::vector<int> tempVals; //temp container
    test_model -> getLocationSpace() -> getLocation(householdLocation -> getId(), tempVals);
    printf("\n\n\nConfirm initial %d, %d\n", tempVals[0], tempVals[1]);

    printf("state before is %d\n", householdLocation -> getState()); //it appears the model does not set state of new location to 1(house)
    householdLocation -> setState(0); //reseting the value of current household residence location to 0 (indicating that it is free to be detected by the code below)
    printf("state after is %d\n", householdLocation -> getState());
  }

  if (debug) { //create a pointer to the SharedDiscreteSpace
    repast::SharedDiscreteSpace<Location, repast::StrictBorders, repast::SimpleAdder<Location> >* locationSpace;
  }

	repast::Moore2DGridQuery<Location> moore2DQuery(test_model -> getLocationSpace()); //to query area around new new (current) field
	int range = 10; //1000/100 or max.Distance/100 (100 meters per cell)
	int i = 1; //loop handler

	//get the expected location theat complies with all conditions
	LocationSearch:
    suitableLocations.clear();
    suitableLocationsRelax_C.clear();
    suitableLocationsRelax_A_C.clear();
    finalSuitableLocations.clear(); //clear these to avoid duplicates when looping

		moore2DQuery.query(field, (range * i), false, neighbouringLocations); //get locations around field
		for (std::vector<Location*>::iterator it = neighbouringLocations.begin() ; it != neighbouringLocations.end(); ++it){ //loop through identified lcaotions
			Location* tempLoc = (&**it); //copy location pointed at by (it) to tempLoc
      std::vector<int> tempPoints; //to store temp location's coordinates

      test_model -> getLocationSpace() -> getLocation((&**it) -> getId(), tempPoints); //get location and save the coordinates
      double _tempDistance = sqrt(pow((tempPoints[0] - field[0]), 2) + pow((tempPoints[1] - field[1]), 2)); //get distance between field and temp location
      int _tempFldtHarvest = (&**it) -> getExpectedHarvest(); //get expected harvest for temp location

      if (debug) {
        printf("\n\nTempLocation at location (%d, %d) is (%0.3f) Km away from field's location.\n", tempPoints[0], tempPoints[1], _tempDistance);
        printf("\t- has an expected harvest of (%d).\n", _tempFldtHarvest);
        printf("\t- has a state of (%d).\n", tempLoc -> getState());
      }

      if(tempLoc -> getWater()){ //assuming a water source can be a field too
        waterSources.push_back(tempLoc); //copy the current location to the water source vector if it has water

        if (debug) {
          printf("\n\nWater source but not a field. Range is: %d\n", (range * i));
        }
      }

			if(tempLoc -> getState() != 2){ //condition B: not a field
        if (_tempDistance <= 10) { //condition A: with one kilometer (10 cells)
          if (!(_tempFldtHarvest > _currentFldHarvest)) { //condition C: has no more harvest than the assigned field
            suitableLocations.push_back(tempLoc); //if conditions A, B and C are satisfied

            if (debug) {
              printf("A, B and C satisfied.\n");
            }
          } else { //relax C
            suitableLocationsRelax_C.push_back(tempLoc); //if conditions A and B are satisfied, but C was relaxed

            if (debug) {
              printf("A and B satisfied.\n");
            }
          }
        } else { //relax A and C
          suitableLocationsRelax_A_C.push_back(tempLoc); //if conditions B is satisfied, but A and C were relaxed

          if (debug) {
            printf("nOnly B satisfied.\n");
          }
        }
				if(tempLoc -> getWater()){ //assuming water sources can not be a field?
					waterSourcesNotField.push_back(tempLoc); //copy the current location to the water source vector if it has water

          if (debug) {
            printf("\n\nWater source but not a field.\n");
          }
				}
			}
		}

    if (suitableLocations.size() > 0) { //try to get all locations satisfying all conditions
      finalSuitableLocations = suitableLocations;

      if (debug) {
        printf("\nLocations satisfying all conditions detected.\n");
        printf("Number of locations detected: %d/%d.\n", suitableLocations.size(), finalSuitableLocations.size());
      }
    } else if (suitableLocationsRelax_C.size() > 0) { //try to get locations relaxing C
      finalSuitableLocations = suitableLocationsRelax_C;

      if (debug) {
        printf("\nLocations with condition C relaxed detected.\n");
        printf("Number of locations detected: %d/%d.\n", suitableLocationsRelax_C.size(), finalSuitableLocations.size());
      }
    } else if (suitableLocationsRelax_A_C.size() > 0) { //try to get locations relaxing A and C
      finalSuitableLocations = suitableLocationsRelax_A_C;

      if (debug) {
        printf("\nLocations satisfying only conditions B detected.\n");
        printf("Number of locations detected: %d/%d.\n", suitableLocationsRelax_A_C.size(), finalSuitableLocations.size());
      }
    }

    if (debug) { //print number of cells suitable and with water and print the co-ordinates of each:
      printf("\n\n\nNumber of suitable locations %d.\n", suitableLocations.size());
      printf("\n\n\nNumbe of water source cells %d.\n", waterSources.size());

      int j = 0;
      std::vector<int> tempVals; //temp container

      for (std::vector<Location*>::iterator it1 = suitableLocations.begin(); it1 != suitableLocations.end(); ++it1){
        tempVals.clear();
        Location* tempLoc = (&**it1);
        test_model -> getLocationSpace() -> getLocation((&**it1)->getId(), tempVals);
        printf("\n\n\nSuitable location %d::: %d, %d \twater is %s \t yield: %d\n", i, tempVals[0], tempVals[1], tempLoc -> getWater() ? "there" : "not there", tempLoc -> getExpectedYield() );
        j++;
      }

      j = 0;

      for (std::vector<Location*>::iterator it1 = waterSources.begin(); it1 != waterSources.end(); ++it1){
        tempVals.clear();
        Location* tempLoc = (&**it1);
        test_model -> getLocationSpace() -> getLocation((&**it1)->getId(), tempVals);
        printf("\n\n\nWater location %d::: %d, %d \twater is %s.\n", i, tempVals[0], tempVals[1], tempLoc -> getWater() ? "there" : "not there");
        j++;
      }
    }

		if(finalSuitableLocations.size() == 0 || waterSources.size() == 0){ //if no suitable location or water source was identified
      if ((range * i) > 120) {// boardSizeY = 120. if current is wider than the simulated region
        goto exitLoop; //jump outside the loop
      }
      i++; //widen the search range
			goto LocationSearch; //loop back to search again after i was incremented
		}	else if (finalSuitableLocations.size() == 1){ //if one suitable location and one or more water sources were identified
			std::vector<int> loc2;
			test_model -> getLocationSpace() -> getLocation(suitableLocations[0] -> getId(), loc2);
      expectedSuitableLocation = finalSuitableLocations[0]; //the only suitable location is copied to the expectedSuitableLocation object

      if(debug){
        printf("\n\n\nOnly one suitableLocations at ::: (%d, %d) ...\n", loc2[0], loc2[1]);
      }
		} else { //condition E: if more than one suitable location and one or more water sources were identified
      std::vector<int> point1, point2;
      std::vector<double> distances;
      for (std::vector<Location*>::iterator it1 = finalSuitableLocations.begin() ; it1 != finalSuitableLocations.end(); ++it1){ //iterator to iterate through identified suitable locations
        test_model -> getLocationSpace() -> getLocation((&**it1) -> getId(), point1); //save coordinates to point1

        //printf("\n\n\nFor suitable location ::: (%d, %d): water sources and distance to each is:\n", point1[0], point1[1]);

        for (std::vector<Location*>::iterator it2 = waterSources.begin() ; it2 != waterSources.end(); ++it2){ //iterator to iterate through identified water sources
          test_model -> getLocationSpace() -> getLocation((&**it2) -> getId(), point2); //save coordinates to point2
          double distance = sqrt(pow((point1[0] - point2[0]), 2) + pow((point1[1] - point2[1]), 2)); //get the distance between water sources and suitable locations
          distances.push_back(distance); //save the distance in vector distances

          if(debug){
            printf("\nFor suitable location: (%d, %d): water sources and distance to each is:\n-water source at: (%d, %d) and distance is: %0.3f.\n", point1[0], point1[1], point2[0], point2[1], distance);
          }
        }
      }

      int minElementIndex = std::min_element(distances.begin(), distances.end()) - distances.begin(); //get the index of the minimum element in vector distances (lowest distance) and check how far it is from the begining of vector distances
      minElementIndex = minElementIndex / waterSources.size(); //devide the index of the minimum distance by number of watersources to identify the location closest to the water source (simply put: we scale down the index we got from distances vector to its correspoding index in finalSuitableLocations vector).
      //number of elements in distances = waterSources * finalSuitableLocations. Hence, finalSuitableLocations = distances / waterSources
      expectedSuitableLocation = finalSuitableLocations[minElementIndex]; //save the object satisfying condition E
      std::vector<int> loc2;
      test_model -> getLocationSpace() -> getLocation(expectedSuitableLocation -> getId(), loc2);
      waterDistances = distances;

      if (debug) {//print location of expected suitable location
        printf("\n\n\n -- Chosen only one at ::: (%d, %d) ...\n", loc2[0], loc2[1]);
        printf("\n -- Number water sources identified is %d.\n", waterSources.size());
        printf("\n -- Number water sources (not in a field) identified is %d.\n", waterSources.size());
        minElementIndex = std::min_element(distances.begin(), distances.end()) - distances.begin();
        //printf("%d in %d elements", minElementIndex, distances.size());
        std::vector<int> loc3;
        test_model -> getLocationSpace() -> getLocation(waterSources[minElementIndex / distances.size()] -> getId(), loc3);
        printf("\n \t--distance between selected loaction and water (%d, %d) source is %.3f.\n", distances[minElementIndex], loc3[0], loc3[1]);
      }
		}

  exitLoop: //continue code if expected to leave the valley

  std::vector<int> expectedSuitablePoints; //store expectedSuitableLocation pints
  std::vector<int> nearestWaterSourcePoints; //store nearest water source to a suitable location
  test_model -> getLocationSpace() -> getLocation(expectedSuitableLocation -> getId(), expectedSuitablePoints); //get expected suitableLocation points
  int minElementIndex = std::min_element(waterDistances.begin(), waterDistances.end()) - waterDistances.begin(); //get the min element again, outside the loop
  test_model -> getLocationSpace() -> getLocation(waterSources[minElementIndex / waterDistances.size()] -> getId(), nearestWaterSourcePoints); //get coordinates of nearest water source

  double distanceWaterToReturnedResd = sqrt(pow((residence[0] - nearestWaterSourcePoints[0]), 2) + pow((residence[1] - nearestWaterSourcePoints[1]), 2));
  double distanceWaterToExpectedResd = sqrt(pow((expectedSuitablePoints[0] - nearestWaterSourcePoints[0]), 2) + pow((expectedSuitablePoints[1] - nearestWaterSourcePoints[1]), 2));
  double distanceFieldToExpectedResd = sqrt(pow((expectedSuitablePoints[0] - field[0]), 2) + pow((expectedSuitablePoints[1] - field[1]), 2));

  if (debug) { //same as the if above.
    printf("\n\n\n -- Chosen only one at ::: (%d, %d) ...\n", expectedSuitablePoints[0], expectedSuitablePoints[1]);
    printf("\n -- Number water sources identified is %d.\n", waterSources.size());
    printf("\n -- Number water sources (not in a field) identified is %d.\n", waterSources.size());
    //printf("%d in %d elements", minElementIndex, waterDistances.size());
    printf("\n \t--distance between selected loaction and water (%d, %d) source is %.3f.\n", waterDistances[minElementIndex], nearestWaterSourcePoints[0], nearestWaterSourcePoints[1]);
  }

  bool testPassed = false;

  if (suitableLocations.size() >= 1) {
    if ((expectedSuitablePoints[0] == residence[0]) && (expectedSuitablePoints[1] == residence[1])) {
      closestToWater = true;
    } else {
      closestToWater = false;
    }

    printf("\nRule_4:\nThe nearest water source was found at (%d, %d).\n\n--Expected: new residence to be at (%d, %d) which has a yield of (%d), state (%d), about (%.3f) Km away from the new field location, and about (%.3f) Km away from the nearesr water source. \n\n--Returned: household moved to (%d, %d) which has a yield of (%d), state (%d), about (%.3f) Km from new field location, and about (%.3f) Km from nearesr water source. \n\t  ---- Rule_4 was %s ---- \n\n", nearestWaterSourcePoints[0], nearestWaterSourcePoints[1], expectedSuitablePoints[0], expectedSuitablePoints[1], expectedSuitableLocation -> getExpectedHarvest(), expectedSuitableLocation -> getState(), distanceFieldToExpectedResd / 10, distanceWaterToExpectedResd / 10, residence[0], residence[1], householdLocation -> getExpectedHarvest(), householdLocation -> getState(), distanceFromNew / 10, distanceWaterToReturnedResd / 10, closestToWater ? "obeyed" : "not obeyed");

  } else {
    printf("\n\n  ---- Household should have left the valley ----  \n\n");
  }

  if (finalSuitableLocations.size() > 1) {
    if (notField && closestToWater) {
      if (residenceLowerYield) {
        if (withinOneKilometer) {
          testPassed = true;
          printf("\n\n  ---- Confirm-Agent-Moves unit test passed successfully, after satisfying all conditions. ----  \n\n");
        }
        testPassed = true;
        printf("\n\n  ---- Confirm-Agent-Moves unit test passed successfully, after relaxing condition 3. ----  \n\n");
      }
      printf("\n\n  ---- Confirm-Agent-Moves unit test passed successfully, after relaxing conditions 1 and 3.  ----  \n\n");
    } else if (notField && !closestToWater) {
      printf("\n\n  ---- Confirm-Agent-Moves unit test failed for violating condition 4. ----  \n\n");
    } else if (!notField) {
      printf("\n\n  ---- Confirm-Agent-Moves unit test failed for violating condition 2. ----  \n\n");
    }
  } else if (finalSuitableLocations.size() == 1) {
    if (notField) {
      testPassed = true;
      printf("\n\n  ---- Confirm-Agent-Moves unit test passed successfully, after satisfying only condition 2. ----  \n\n");
    }
  } else {
    printf("\n\n ---- Household should have left the valley ---- \n\n");
  }

  //delete the model:
  delete test_model;

  //declare end of test:
  printf("\n---- End of Confirm-Agent-Moves unit test ----\n\n");
  return testPassed;
}

/*
Run the program with the inclusion of the tests - not sure
if this is the correct way to go about running the tests
*/
int main(int argc, char** argv){
  std::string configFile = argv[1]; // The name of the configuration file
	std::string propsFile  = argv[2]; // The name of the properties file

	boost::mpi::environment env(argc, argv);
	boost::mpi::communicator* world;

	repast::RepastProcess::init(configFile);
  world = new boost::mpi::communicator;

  bool t1 = testInstantiateAgent();
  bool t2 = testUpdateEnv(argc, argv);  
  bool t3 = testReadDeathAge(argc, argv, world);
  bool t4 = testGenOutputFile( propsFile, argc, argv, world );
  bool t5 = testManipulateAgentField(argc, argv, world);
  bool t6 = testConfirmAgentMoves(propsFile, argc, argv, world );

  bool tests[] = {t1, t2, t3, t4, t5, t6};
  string testNames[] = {
    "Instantiate an agent",
    "Update the environment with new archaeological data",
    "Read a new ‘death age’ parameter at run-time",
    "Generate an output file",
    "Manipulate an agent’s field cell",
    "Trigger an agent to move its residence"
  };

  time_t now = time(0);
  
  // convert now to string form
  char* dt = ctime(&now);

  // convert now to tm struct for UTC
  tm *gmtm = gmtime(&now);

  dt = asctime(gmtm);

  ofstream testsOutput("TestResults.txt");
  
  testsOutput<<dt<<"\n";
  testsOutput<<"Component 1 test results: \n\n";
  int testsFailed = 0;
  for (int i=0; i < sizeof(tests); i++) {
    testsOutput<<"Test"<<" ";
    testsOutput<<i+1<<". ";
    testsOutput<<testNames[i]<<":\n";
    if (tests[i]) {
      testsOutput<<"***Passed***"<<"\n\n";
    } else {
      testsFailed += 1;
      testsOutput<<"***Failed***"<<"\n\n";
    }
  }
  if (testsFailed>0){
    testsOutput<<testsFailed<<" ";
    testsOutput<<"test(s) failed.";
  } else {
    testsOutput<<"All tests passed."; 
  }
  testsOutput.close();
}
