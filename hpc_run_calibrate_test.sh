#!/bin/bash

#SBATCH --mail-user=tgoldsack1@sheffield.ac.uk
#SBATCH --mail-type=ALL
#SBATCH --time=20:00:00
#SBATCH --comment=test_script

module use /usr/local/modulefiles/staging/eb/all/
module load RepastHPC-Boost1.73.0/2.3.0-foss-2018b
module load R/4.0.0-foss-2020a

Rscript calibrate.test.r