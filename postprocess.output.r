## post simulation output:
library(here)

# read model outputs:
simout <- read.csv(here("NumberOfHousehold.csv"), header = TRUE)

# get target years to get the number of expected values:
target_data <- read.csv(here("data", "target_data.csv"), header = FALSE)
target_years <- target_data[[1]]

# get the required column:
outTarget <- simout[, 2]

length(outTarget) <- length(target_years)

# write data to output:
write.table(t(outTarget), file = "output", sep = " ", row.names = FALSE, col.names = FALSE)
