install.packages("here", repos='http://cran.us.r-project.org')
install.packages("EasyABC", repos='http://cran.us.r-project.org')
install.packages("properties", repos='http://cran.us.r-project.org')

library(here)
library(EasyABC)

# Learning how to use R - get target data
target_data <- read.csv(here("data", "target_data.csv"), header = FALSE)
target_years <- target_data[[1]]
target_numHouseholds <- target_data[[2]]

# Get model data
model_data <- read.csv(here("NumberOfHousehold.csv"))
model_numHouseholds <- model_data[[2]]

# Plot
#plot(target_years, target_numHouseholds, type="l", col="red")
#lines(target_years, model_numHouseholds, type="l")
#plot(target_years, model_numHouseholds, type="l")
#lines(target_years, target_numHouseholds, type="l", col="red")

# Use abc package

# Define changing parameters (priors) from distribution

# for now, just use the village properties

prior=list(c("unif", 16, 20.9999), #min.fission.age 
           c("unif", 28, 34.9999), #max.fission.age 
           c("unif", 25, 30.9999), #min.death.age
           c("unif", 35, 40.9999), #max.death.age
           c("unif", 0.01, 0.11), #fertility.prop
           c("unif", 0.75, 0.99), #harvest.adj
           c("unif", 0.75, 1.25), #fission.multiplier
           c("unif", 3, 10.9999), #max.capacity
           c("unif", 0.01, 1), #formation.prob
           c("unif", 1, 5.9999)) #radius

# Notes from tuong
# fastsimcoal in documentatio
# prior = list of priors
# inTarget = target data
# Rscript proprocess converts input from abc into a model.props
# Rscript Postprocess script converts the output 
# output should match the inTarget


# Flow:
# Easy ABC creates input file - pass easy ABC a shell script
# We preprocess this (using an r file) to create a model.props
# Run the model
# Postprocess the file (using another r file) if necessary to create output file
# This is then read again by abc

# maybe use properties file for model.props creation
nsim <- 50 # Number of simulations to run
tol <- 0.5 # Tolerance rate
inTarget = as.numeric(unlist(target_numHouseholds))

ABC_sim <- ABC_rejection(binary_model("./abc_run_sim.sh"), prior=prior, nb_simul=nsim, 
                         n_cluster=1, use_seed=TRUE, seed_count=0,
                         tol=tol, summary_stat_target=inTarget)#, prior_test="X8>(2*X10+1)^2",)

# # write ABC_sim to storage:
#r = sample(1:1000000, 1)
r = 50
saveRDS(ABC_sim, file = paste("acse6312_group_6_test_calibration_", r[1]))