#!/bin/bash

# Update environment paths
# export PATH=$HOME/sfw/MPICH/bin:$PATH
# export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/$HOME/sfw/Boost/Boost_1.61/lib/:/$HOME/sfw/repast_hpc-2.3.0/lib/

# Copy the inputs file
cp ./input ./input.temp

# Preprocess: input -> model.props
Rscript preprocess.input.r

# Run model
mpirun -n 1 ./bin/main.exe ./props/config.props ./props/model.props

# Postprocess: NumberOfHouseholds.csv -> output
Rscript postprocess.output.r

# Copy the output file
#cp ./output ./output.temp
