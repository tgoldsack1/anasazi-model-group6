install.packages("properties", repos="http://cran.r-project.org")
library(here)
library(properties)

# Read input file from easy abc
#fromEasyABC<-read.delim('input.temp',header=FALSE)
fromEasyABC<-read.delim('input',header=FALSE)
#randomSeed <- as.integer(fromEasyABC[1,])
#set.seed(randomSeed)

# Define the props being calibrated (in order given to Easy_ABC)
changed_props = list(min.fission.age = as.integer(fromEasyABC[2,]),
                     max.fission.age = as.integer(fromEasyABC[3,]),
                     min.death.age = as.integer(fromEasyABC[4,]),
                     max.death.age = as.integer(fromEasyABC[5,]),
                     fertility.prop = fromEasyABC[6,],
                     harvest.adj = fromEasyABC[7,],
                     village.fission.multiplier = fromEasyABC[8,],
                     village.max.capacity = as.integer(fromEasyABC[9,]),
                     village.formation.prob = fromEasyABC[10,],
                     village.radius = as.integer(fromEasyABC[11,]))

# Define the props not being calibrated
default_props <- list(random.seed = 99,
 count.of.agents = 14, board.size.x = 80,
 board.size.y = 120,proc.per.x = 1,proc.per.y = 1,
 grid.buffer = 0, start.year = 800,end.year = 1350,
 max.store.year = 2,max.storage = 1600,household.need = 800,
 max.distance = 1000,initial.min.corn = 1000,
 initial.max.corn = 1600,annual.variance = 0.10000,
 spatial.variance = 0.10000,new.household.ini.maize = 0.33000,
 result.file = "NumberOfHousehold.csv")

# Alternative way of reading from the props file:
#baseCaseProps = read.delim("props/model.props", sep = "=", header = FALSE)
#baseCaseProps = baseCaseProps[1:24,] # Remove village parameters under calibration
#baseCaseProps = split(baseCaseProps$V2, baseCaseProps$V1) # Create a list
#baseCaseProps = lapply(baseCaseProps, as.numeric) # Does not seem to affect the process

# combining the properties into one list
props = c(default_props, changed_props)
#props = c(baseCaseProps, changed_props)

# Writing list to a file
write.properties(file = "./props/model.props", properties = props)
