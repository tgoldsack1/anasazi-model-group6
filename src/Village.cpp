#include "Village.h"
#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedDiscreteSpace.h"
#include "Location.h"
#include "Household.h"
#include <vector>
/* #include "repast_hpc/Random.h" */

Village::Village( repast::AgentId id, Location* centreLocation, std::vector<Household*> foundingHouseholds) {
  villageId = id;
  centre = centreLocation;
  households = foundingHouseholds;
  //setVillageSpace( centre, locationSpace );
}

Village::~Village() {}

// Methods:
int Village::getCurrentCapacity() {
  return households.size();
}

void Village::addHousehold( Household* household ) {  

  households.push_back(household);
}

void Village::removeHousehold( repast::AgentId householdId ) {

  if( households.size() > 0 ) {
    for ( vector<Household*>::iterator iterator = households.begin(); iterator != households.end(); ++iterator ) { //iterator to iterate through households in vector households
      if ( ( (&**iterator) -> getId() ) ==  householdId ) {
        households.erase(iterator);
        goto exitLoop;
      }
    }
    exitLoop:;
  } else {
    printf("No households to remove from village!\n");
  }

}

void Village::redistributeMaize(int needs) {
  int totalNeeds = ( households.size() * needs ); //needs = param.householdNeed
  int sumStoredMaize = 0; //sum of all stored maize in the villagers residences
  int sumExpectedYeild = 0; //sum of all expected yeild from residents' fields
  int allMaize = 0;
  int sharedStoredMaize = 0; //average stored maise (equals to needs or average of all maze)
  int sharedHarvest = 0; //average harvest (average of allMaize - sharedStoredMaize, if any)
  
  //get stored maize and expected harvest:
  if ( households.size() > 0 ) {
    for ( vector<Household*>::iterator iterator = households.begin(); iterator != households.end(); ++iterator ) { //iterator to iterate through households in vector households
      sumStoredMaize += (&**iterator) -> getMaizeStorage();
      sumExpectedYeild += (&**iterator) -> getAssignedField() -> getExpectedYield();
    }

    sumStoredMaize /= households.size();
    sumExpectedYeild /= households.size();

    //set distributed values:
    for ( vector<Household*>::iterator iterator = households.begin(); iterator != households.end(); ++iterator ) { //iterator to iterate through households in vector households
      (&**iterator) -> setMaizeStorage( sumStoredMaize );
      (&**iterator) -> getAssignedField() -> setExpectedYield( sumExpectedYeild );
    }

  } else {
    printf("No households in village!\n");
  }
}
