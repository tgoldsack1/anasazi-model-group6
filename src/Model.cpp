#include <stdio.h>
#include <vector>
#include <cmath>
#include <boost/mpi.hpp>
#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/initialize_random.h"
#include "repast_hpc/SVDataSetBuilder.h"
#include "repast_hpc/Point.h"
#include "repast_hpc/Random.h"
#include "repast_hpc/Schedule.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedDiscreteSpace.h"
#include "repast_hpc/GridComponents.h"
#include <string>
#include <fstream>
#include <stdlib.h>
#include "repast_hpc/Moore2DGridQuery.h"

#include "Model.h"

// substracts b<T> to a<T>
template <typename T>
void substract_vector(std::vector<T>& a, const std::vector<T>& b)
{
	typename std::vector<T>::iterator       it = a.begin();
	typename std::vector<T>::const_iterator it2 = b.begin();

	while (it != a.end())
	{
		while (it2 != b.end() && it != a.end())
		{
			if (*it == *it2)
			{
				it = a.erase(it);
				it2 = b.begin();
			}

			else
				++it2;
		}
		if (it != a.end())
			++it;

		it2 = b.begin();
	}
}

AnasaziModel::AnasaziModel(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm): context(comm) , locationContext(comm), villageContext(comm) // implementing proposed improvements:
{
	props = new repast::Properties(propsFile, argc, argv, comm);
	boardSizeX = repast::strToInt(props->getProperty("board.size.x"));
	boardSizeY = repast::strToInt(props->getProperty("board.size.y"));

	initializeRandom(*props, comm);
	repast::Point<double> origin(0,0);
	repast::Point<double> extent(boardSizeX, boardSizeY);
	repast::GridDimensions gd (origin, extent);

	int procX = repast::strToInt(props->getProperty("proc.per.x"));
	int procY = repast::strToInt(props->getProperty("proc.per.y"));
	int bufferSize = repast::strToInt(props->getProperty("grid.buffer"));

	std::vector<int> processDims;
	processDims.push_back(procX);
	processDims.push_back(procY);
	householdSpace = new repast::SharedDiscreteSpace<Household, repast::StrictBorders, repast::SimpleAdder<Household> >("AgentDiscreteSpace",gd,processDims,bufferSize, comm);
	locationSpace = new repast::SharedDiscreteSpace<Location, repast::StrictBorders, repast::SimpleAdder<Location> >("LocationDiscreteSpace",gd,processDims,bufferSize, comm);

	context.addProjection(householdSpace);
	locationContext.addProjection(locationSpace);

	/* -------------------------- */ //implementing proposed improvements
	villageSpace = new repast::SharedDiscreteSpace<Village, repast::StrictBorders, repast::SimpleAdder<Village> >("VillageDiscreteSpace",gd,processDims,bufferSize, comm); // implementing proposed improvements:
	villageContext.addProjection(villageSpace);
	/* -------------------------- */ //implementing proposed improvements

	param.startYear = repast::strToInt(props->getProperty("start.year"));
	param.endYear = repast::strToInt(props->getProperty("end.year"));
	param.maxStorageYear = repast::strToInt(props->getProperty("max.store.year"));
	param.maxStorage = repast::strToInt(props->getProperty("max.storage"));
	param.householdNeed = repast::strToInt(props->getProperty("household.need"));
	param.minFissionAge = repast::strToInt(props->getProperty("min.fission.age"));
	param.maxFissionAge = repast::strToInt(props->getProperty("max.fission.age"));
	param.minDeathAge = repast::strToInt(props->getProperty("min.death.age"));
	param.maxDeathAge = repast::strToInt(props->getProperty("max.death.age"));
	param.maxDistance = repast::strToInt(props->getProperty("max.distance"));
	param.initMinCorn = repast::strToInt(props->getProperty("initial.min.corn"));
	param.initMaxCorn = repast::strToInt(props->getProperty("initial.max.corn"));

	param.annualVariance = repast::strToDouble(props->getProperty("annual.variance"));
	param.spatialVariance = repast::strToDouble(props->getProperty("spatial.variance"));
	param.fertilityProbability = repast::strToDouble(props->getProperty("fertility.prop"));
	param.harvestAdjustment = repast::strToDouble(props->getProperty("harvest.adj"));
	param.maizeStorageRatio = repast::strToDouble(props->getProperty("new.household.ini.maize"));

	// Village props
	param.villageFissionMultiplier = repast::strToDouble(props->getProperty("village.fission.multiplier"));
	param.villageMaxCapacity = repast::strToDouble(props->getProperty("village.max.capacity"));
	param.villageFormationProb = repast::strToDouble(props->getProperty("village.formation.prob"));
	param.villageRadius = repast::strToDouble(props->getProperty("village.radius"));

	year = param.startYear;
	stopAt = param.endYear - param.startYear + 1;
	fissionGen = new repast::DoubleUniformGenerator(repast::Random::instance()->createUniDoubleGenerator(0,1));
	deathAgeGen = new repast::IntUniformGenerator(repast::Random::instance()->createUniIntGenerator(param.minDeathAge,param.maxDeathAge));
	yieldGen = new repast::NormalGenerator(repast::Random::instance()->createNormalGenerator(0,param.annualVariance));
	soilGen = new repast::NormalGenerator(repast::Random::instance()->createNormalGenerator(0,param.spatialVariance));
	initAgeGen = new repast::IntUniformGenerator(repast::Random::instance()->createUniIntGenerator(0,param.minDeathAge));
	initMaizeGen = new repast::IntUniformGenerator(repast::Random::instance()->createUniIntGenerator(param.initMinCorn,param.initMaxCorn));

	string resultFile = props->getProperty("result.file");
	out.open(resultFile);
	out << "Year,Number-of-Households" << endl;
}

AnasaziModel::~AnasaziModel()
{
	delete props;
	out.close();
}

void AnasaziModel::initAgents()
{
	int rank = repast::RepastProcess::instance()->rank();

	int LocationID = 0;
	for(int i=0; i<boardSizeX; i++ )
	{
		for(int j=0; j<boardSizeY; j++)
		{
			repast::AgentId id(LocationID, rank, 1);
			Location* agent = new Location(id, soilGen->next());
			locationContext.addAgent(agent);
			locationSpace->moveTo(id, repast::Point<int>(i, j));
			LocationID++;
		}
	}

	readCsvMap();
	readCsvWater();
	readCsvPdsi();
	readCsvHydro();
	int noOfAgents  = repast::strToInt(props->getProperty("count.of.agents"));
	repast::IntUniformGenerator xGen = repast::IntUniformGenerator(repast::Random::instance()->createUniIntGenerator(0,boardSizeX-1));
	repast::IntUniformGenerator yGen = repast::IntUniformGenerator(repast::Random::instance()->createUniIntGenerator(0,boardSizeY-1));
	for(int i =0; i< noOfAgents;i++)
	{
		repast::AgentId id(houseID, rank, 2);
		int initAge = initAgeGen->next();
		int mStorage = initMaizeGen->next();
		Household* agent = new Household(id, initAge, deathAgeGen->next(), mStorage);
		context.addAgent(agent);
		std::vector<Location*> locationList;

		newLocation:
		int x = xGen.next();
		int y = yGen.next();
		locationSpace->getObjectsAt(repast::Point<int>(x, y), locationList);

		if(locationList[0]->getState()==2)
		{
			locationList.clear();
			goto newLocation;
		}
		else
		{
			householdSpace->moveTo(id, repast::Point<int>(x, y));
			locationList[0]->setState(1);
		}


		houseID++;
	}

	// initialise village as agent
	repast::AgentId id(villageID, rank, 3);


	updateLocationProperties();

	repast::SharedContext<Household>::const_iterator local_agents_iter = context.begin();
	repast::SharedContext<Household>::const_iterator local_agents_end = context.end();

	while(local_agents_iter != local_agents_end)
	{
		Household* household = (&**local_agents_iter);
		if(household->death())
		{
			repast::AgentId id = household->getId();
			local_agents_iter++;

			std::vector<int> loc;
			householdSpace->getLocation(id, loc);

			std::vector<Location*> locationList;
			if(!loc.empty())
			{
				locationSpace->getObjectsAt(repast::Point<int>(loc[0], loc[1]), locationList);
				locationList[0]->setState(0);
			}
			context.removeAgent(id);
		}
		else
		{
			local_agents_iter++;
			fieldSearch(household);
		}
	}
}

std::vector<Household*> AnasaziModel::getAgents()
{
	std::vector<Household*> agents;
	context.selectAgents(context.size(), agents);
	return agents;
}

std::vector<Location*> AnasaziModel::getLocations()
{
	std::vector<Location*> locations;
	locationContext.selectAgents(locationContext.size(), locations);
	return locations;
}

void AnasaziModel::doPerTick()
{
	updateLocationProperties();
	writeOutputToFile();
	year++;
	//std:cout<<year<<"\n";
	updateVillageProperties(); 	// implementing proposed improvements
	updateHouseholdProperties();
}

void AnasaziModel::initSchedule(repast::ScheduleRunner& runner)
{
	runner.scheduleEvent(1, 1, repast::Schedule::FunctorPtr(new repast::MethodFunctor<AnasaziModel> (this, &AnasaziModel::doPerTick)));
	runner.scheduleStop(stopAt);
}

void AnasaziModel::readCsvMap()
{
	int x,y,z,mz;
	string zone, maizeZone, temp;

	std::ifstream file ("data/map.csv");//define file object and open map.csv
	file.ignore(500,'\n');//Ignore first line

	while(1)//read until end of file
	{
		getline(file,temp,',');
		if(!temp.empty())
		{
			x = repast::strToInt(temp); //Read until ',' and convert to int & store in x
			getline(file,temp,',');
			y = repast::strToInt(temp); //Read until ',' and convert to int & store in y
			getline(file,temp,','); //colour
			getline(file,zone,',');// read until ',' and store into zone
			getline(file,maizeZone,'\n');// read until next line and store into maizeZone
			if(zone == "\"Empty\"")
			{
				z = 0;
			}
			else if(zone == "\"Natural\"")
			{
				z = 1;
			}
			else if(zone == "\"Kinbiko\"")
			{
				z = 2;
			}
			else if(zone == "\"Uplands\"")
			{
				z = 3;
			}
			else if(zone == "\"North\"")
			{
				z = 4;
			}
			else if(zone == "\"General\"")
			{
				z = 5;
			}
			else if(zone == "\"North Dunes\"")
			{
				z = 6;
			}
			else if(zone == "\"Mid Dunes\"")
			{
				z = 7;
			}
			else if(zone == "\"Mid\"")
			{
				z = 8;
			}
			else
			{
				z = 99;
			}

			if(maizeZone.find("Empty") != std::string::npos)
			{
				mz = 0;
			}
			else if(maizeZone.find("No_Yield") != std::string::npos)
			{
				mz = 1;
			}
			else if(maizeZone.find("Yield_1") != std::string::npos)
			{
				mz = 2;
			}
			else if(maizeZone.find("Yield_2") != std::string::npos)
			{
				mz = 3;
			}
			else if(maizeZone.find("Yield_3") != std::string::npos)
			{
				mz = 4;
			}
			else if(maizeZone.find("Sand_dune") != std::string::npos)
			{
				mz = 5;
			}
			else
			{
				mz = 99;
			}
			std::vector<Location*> locationList;
			locationSpace->getObjectsAt(repast::Point<int>(x, y), locationList);
			locationList[0]->setZones(z,mz);
		}
		else{
			goto endloop;
		}
	}
	endloop: ;
}

void AnasaziModel::readCsvWater()
{
	//read "type","start date","end date","x","y"
	int type, startYear, endYear, x, y;
	string temp;

	std::ifstream file ("data/water.csv");//define file object and open water.csv
	file.ignore(500,'\n');//Ignore first line
	while(1)//read until end of file
	{
		getline(file,temp,',');
		if(!temp.empty())
		{
			getline(file,temp,',');
			getline(file,temp,',');
			getline(file,temp,',');
			type = repast::strToInt(temp); //Read until ',' and convert to int
			getline(file,temp,',');
			startYear = repast::strToInt(temp); //Read until ',' and convert to int
			getline(file,temp,',');
			endYear = repast::strToInt(temp); //Read until ',' and convert to int
			getline(file,temp,',');
			x = repast::strToInt(temp); //Read until ',' and convert to int
			getline(file,temp,'\n');
			y = repast::strToInt(temp); //Read until ',' and convert to int

			std::vector<Location*> locationList;
			locationSpace->getObjectsAt(repast::Point<int>(x, y), locationList);
			locationList[0]->addWaterSource(type,startYear, endYear);
			//locationList[0]->checkWater(existStreams, existAlluvium, x, y, year);
		}
		else
		{
			goto endloop;
		}
	}
	endloop: ;
}

void AnasaziModel::readCsvPdsi()
{
	//read "year","general","north","mid","natural","upland","kinbiko"
	int i=0;
	string temp;

	std::ifstream file ("data/pdsi.csv");//define file object and open pdsi.csv
	file.ignore(500,'\n');//Ignore first line

	while(1)//read until end of file
	{
		getline(file,temp,',');
		if(!temp.empty())
		{
			pdsi[i].year = repast::strToInt(temp); //Read until ',' and convert to int
			getline(file,temp,',');
			pdsi[i].pdsiGeneral = repast::strToDouble(temp); //Read until ',' and convert to double
			getline(file,temp,',');
			pdsi[i].pdsiNorth = repast::strToDouble(temp); //Read until ',' and convert to double
			getline(file,temp,',');
			pdsi[i].pdsiMid = repast::strToDouble(temp); //Read until ',' and convert to double
			getline(file,temp,',');
			pdsi[i].pdsiNatural = repast::strToDouble(temp); //Read until ',' and convert to int
			getline(file,temp,',');
			pdsi[i].pdsiUpland = repast::strToDouble(temp); //Read until ',' and convert to int
			getline(file,temp,'\n');
			pdsi[i].pdsiKinbiko = repast::strToDouble(temp); //Read until ',' and convert to double
			i++;
		}
		else{
			goto endloop;
		}
	}
	endloop: ;
}

void AnasaziModel::readCsvHydro()
{
	//read "year","general","north","mid","natural","upland","kinbiko"
	string temp;
	int i =0;

	std::ifstream file ("data/hydro.csv");//define file object and open hydro.csv
	file.ignore(500,'\n');//Ignore first line

	while(1)//read until end of file
	{
		getline(file,temp,',');
		if(!temp.empty())
		{
			hydro[i].year = repast::strToInt(temp); //Read until ',' and convert to int
			getline(file,temp,',');
			hydro[i].hydroGeneral = repast::strToDouble(temp); //Read until ',' and convert to double
			getline(file,temp,',');
			hydro[i].hydroNorth = repast::strToDouble(temp); //Read until ',' and convert to double
			getline(file,temp,',');
			hydro[i].hydroMid = repast::strToDouble(temp); //Read until ',' and convert to double
			getline(file,temp,',');
			hydro[i].hydroNatural = repast::strToDouble(temp); //Read until ',' and convert to int
			getline(file,temp,',');
			hydro[i].hydroUpland = repast::strToDouble(temp); //Read until ',' and convert to int
			getline(file,temp,'\n');
			hydro[i].hydroKinbiko = repast::strToDouble(temp); //Read until ',' and convert to double
			i++;
		}
		else
		{
			goto endloop;
		}
	}
	endloop: ;
}

int AnasaziModel::yieldFromPdsi(int zone, int maizeZone)
{
	int pdsiValue, row, col;
	switch(zone)
	{
		case 1:
			pdsiValue = pdsi[year-param.startYear].pdsiNatural;
			break;
		case 2:
			pdsiValue = pdsi[year-param.startYear].pdsiKinbiko;
			break;
		case 3:
			pdsiValue = pdsi[year-param.startYear].pdsiUpland;
			break;
		case 4:
		case 6:
			pdsiValue = pdsi[year-param.startYear].pdsiNorth;
			break;
		case 5:
			pdsiValue = pdsi[year-param.startYear].pdsiGeneral;
			break;
		case 7:
		case 8:
			pdsiValue = pdsi[year-param.startYear].pdsiMid;
			break;
		default:
			return 0;
	}

	/* Rows of pdsi table*/
	if(pdsiValue < -3)
	{
		row = 0;
	}
	else if(pdsiValue >= -3 && pdsiValue < -1)
	{
		row = 1;
	}
	else if(pdsiValue >= -1 && pdsiValue < 1)
	{
		row = 2;
	}
	else if(pdsiValue >= 1 && pdsiValue < 3)
	{
		row = 3;
	}
	else if(pdsiValue >= 3)
	{
		row = 4;
	}
	else
	{
		return 0;
	}

	/* Col of pdsi table*/
	if(maizeZone >= 2)
	{
		col = maizeZone - 2;
	}
	else
	{
		return 0;
	}

	return yieldLevels[row][col];
}

double AnasaziModel::hydroLevel(int zone)
{
	switch(zone)
	{
		case 1:
			return hydro[year-param.startYear].hydroNatural;
		case 2:
			return hydro[year-param.startYear].hydroKinbiko;
		case 3:
			return hydro[year-param.startYear].hydroUpland;
		case 4:
		case 6:
			return hydro[year-param.startYear].hydroNorth;
		case 5:
			return hydro[year-param.startYear].hydroGeneral;
		case 7:
		case 8:
			return hydro[year-param.startYear].hydroMid;
		default:
			return 0;
	}
}

void AnasaziModel::checkWaterConditions()
{
	if ((year >= 280 && year < 360) or (year >= 800 && year < 930) or (year >= 1300 && year < 1450))
	{
		existStreams = true;
	}
	else
	{
		existStreams = false;
	}

	if (((year >= 420) && (year < 560)) or ((year >= 630) && (year < 680)) or	((year >= 980) && (year < 1120)) or ((year >= 1180) && (year < 1230)))
	{
		existAlluvium = true;
	}
	else
	{
		existAlluvium = false;
	}
}

void AnasaziModel::writeOutputToFile()
{
	out << year << "," <<  context.size() << std::endl;
}

void  AnasaziModel::updateLocationProperties()
{
	checkWaterConditions();
	int x = 0;
	for(int i=0; i<boardSizeX; i++ )
	{
		for(int j=0; j<boardSizeY; j++)
		{
			std::vector<Location*> locationList;
			locationSpace->getObjectsAt(repast::Point<int>(i, j), locationList);
			locationList[0]->checkWater(existStreams,existAlluvium, i, j, year);
			int mz = locationList[0]->getMaizeZone();
			int z = locationList[0]->getZone();
			int y = yieldFromPdsi(z,mz);
			locationList[0]->calculateYield(y, param.harvestAdjustment, yieldGen->next());
		}
	}
}

void AnasaziModel::updateHouseholdProperties()
{
	repast::SharedContext<Household>::const_iterator local_agents_iter = context.begin();
	repast::SharedContext<Household>::const_iterator local_agents_end = context.end();

	while(local_agents_iter != local_agents_end)
	{
		Household* household = (&**local_agents_iter);

		// Set Maize storage to max if it is greater than max.
		int mS = household -> getMaizeStorage();
		int newMS = min(mS, param.maxStorage);
		household -> setMaizeStorage(newMS);

		if(household->death())
		{

			local_agents_iter++;
<<<<<<< HEAD


			// if household is part of a village, and this village is now too small,
			// unnassign village from other households
			if (household->getAssignedVillage()){
				if (household->getAssignedVillage()->getCurrentCapacity() == 2) {
				
					std::vector<Household*> villageHouseholds = household->getAssignedVillage()->getHouseholds();

					std::vector<Household*>::iterator iter;

					for (iter = villageHouseholds.begin(); iter != villageHouseholds.end(); ++iter)
					{
						if (household->getId().id() != (*iter)->getId().id()) {

							if ((*iter) -> getAssignedVillage()){
								if ((*iter) -> getAssignedVillage()->getId().id() == household->getAssignedVillage()->getId().id()){
									(*iter) -> setAssignedVillage(NULL);
								}
							}						
						}
					}
				}
			}

=======
>>>>>>> 2164500572943a20c497619a2a793a07ee2d826a
			removeHousehold(household);
		}
		else
		{

			local_agents_iter++;
			// Agent fission
			double fissionProb = param.fertilityProbability;
			// If household is part of a village, apply the fertility multiplier
			if(household->getAssignedVillage())
			{
				fissionProb = fissionProb * param.villageFissionMultiplier;
			}
			if(household->fission(param.minFissionAge,param.maxFissionAge, fissionGen->next(), fissionProb))
			{

				int rank = repast::RepastProcess::instance()->rank();
				repast::AgentId id(houseID, rank, 2);
				int mStorage = household->splitMaizeStored(param.maizeStorageRatio);
				Household* newAgent = new Household(id, 0, deathAgeGen->next(), mStorage);
				context.addAgent(newAgent);

				std::vector<int> loc;
				householdSpace->getLocation(household->getId(), loc);
				householdSpace->moveTo(id, repast::Point<int>(loc[0], loc[1]));

				fieldSearch(newAgent);
				houseID++;

			}
			

			bool fieldFound = true;
			if(!(household->checkMaize(param.householdNeed)))
			{

				fieldFound = fieldSearch(household);
			}
			if(fieldFound)
			{
				household->nextYear(param.householdNeed);
			}

		}
	}
}

bool AnasaziModel::fieldSearch(Household* household)
{
	/******** Choose Field ********/
	std::vector<int> loc;
	householdSpace->getLocation(household->getId(), loc);
	repast::Point<int> center(loc);
	Village* isVillager = household->getAssignedVillage();

	std::vector<Location*> neighbouringLocations;
	std::vector<Location*> checkedLocations;
	repast::Moore2DGridQuery<Location> moore2DQuery(locationSpace);
	int range = 1;

	bool villageNeedsRemoving = false;

	while(1)
	{
		moore2DQuery.query(loc, range, false, neighbouringLocations);

		for (std::vector<Location*>::iterator it = neighbouringLocations.begin() ; it != neighbouringLocations.end(); ++it)
		{
			Location* tempLoc = (&**it);
			Village* isVillageLocation = tempLoc->getAssignedVillage();
			bool isVillageSpace = isVillageLocation ? (tempLoc->getAssignedVillage()->getCurrentCapacity() < param.villageMaxCapacity) : false;

			if(!isVillageLocation || isVillageSpace) // if the location is not assigned to a village, or is assigned to a village which has space
			{
				bool isSameVillage = isVillager && isVillageLocation ?
					(tempLoc->getAssignedVillage()->getId().id() == household->getAssignedVillage()->getId().id())
					: false;

				bool isVillageBeingAbandoned = false;
				if (isVillageLocation){
					std::vector<Household*> villageHouseholds = tempLoc->getAssignedVillage()->getHouseholds();
					std::vector<Household*>::iterator iter;
					for (iter = villageHouseholds.begin(); iter != villageHouseholds.end(); ++iter)
					{
						if (!(*iter)->getAssignedVillage()) {
							isVillageBeingAbandoned = true;
						}
					}
				}


				if (!isSameVillage && !isVillageBeingAbandoned){ // check if the location is part of a village the agent is leaving
					if(tempLoc->getState() == 0)
					{
						if(tempLoc->getExpectedYield() >= param.householdNeed)
						{
							// Assign the previous field location to empty
							if (household->getAssignedField()) {
								household->getAssignedField()->setState(0);
							}

							std::vector<int> loc;
							locationSpace->getLocation(tempLoc->getId(), loc);
							tempLoc->setState(2);
							household->chooseField(tempLoc);

							// if household is leaving a village, set its assigned village to NULL
							if (isVillager) {

								household->setAssignedVillage(NULL);

								villageNeedsRemoving = true;
							}

							// if location is part of a village that isn't full, assign necessary class variables
							if (isVillageLocation && isVillageSpace) {
								Village* locVillage = tempLoc->getAssignedVillage();
								locVillage->addHousehold(household);
								household->setAssignedVillage(locVillage);

							} else { // else check if a village can be formed

								bool villageFormed = checkVillageFormation(household);
							}
							goto EndOfLoop;
						}
					}
				}
			}
		}
		range++;
		if(range > boardSizeY)
		{
			removeHousehold(household);

			if (villageNeedsRemoving) {
				isVillager -> removeHousehold(household->getId());
			}
			return false;
		}
	}
	EndOfLoop:
	if(range >= 10)
	{

		Village* removeVillage = villageNeedsRemoving ? isVillager : NULL; 
		bool reloc = relocateHousehold(household, removeVillage);

		return reloc;
	}
	else
	{
		return true;
	}
}

bool AnasaziModel::checkVillageFormation(Household* household)
{
	repast::Moore2DGridQuery<Location> moore2DQuery(locationSpace);
	std::vector<int> fieldCoords;
  locationSpace->getLocation(household->getAssignedField()->getId(), fieldCoords);
	std::vector<Location*> neighbouringLocations;
	moore2DQuery.query(fieldCoords, 2, false, neighbouringLocations);


	// check if there is an agent that meets the forming criteria
	// must have a field a distance of 2 cells away in the x or y direction
	std::vector<Location*> suitableAgentFieldLocations;
	for (std::vector<Location*>::iterator it = neighbouringLocations.begin() ; it != neighbouringLocations.end(); ++it)
	{
		Location* tempLoc = (&**it);
		if (tempLoc->getState() == 2) // if location is a field
		{
			std::vector<int> tempLocCoords;
  		locationSpace->getLocation(tempLoc->getId(), tempLocCoords);

			bool isSameHorizontalAxis = fieldCoords.at(1) == tempLocCoords.at(1);
			bool isSameVerticalAxis =  fieldCoords.at(0) == tempLocCoords.at(0);
			int horizontalDist = abs(fieldCoords.at(0) - tempLocCoords.at(0));
			int verticalDist = abs(fieldCoords.at(1) - tempLocCoords.at(1));
			
			double r = ((double) rand() / (1));
			
			bool meetsCriteria = ((isSameHorizontalAxis && horizontalDist == 2) || (isSameVerticalAxis && verticalDist == 2)) && (r < param.villageFormationProb);

			if (meetsCriteria)
			{
				suitableAgentFieldLocations.push_back(tempLoc);
			}
		}
	}

	if (suitableAgentFieldLocations.size() == 0)
	{
		return false;
	}

	for (std::vector<Location*>::iterator it = suitableAgentFieldLocations.begin() ; it != suitableAgentFieldLocations.end(); ++it)
	{
		// Work out village center coords
		Location* otherAgentFieldLoc = (&**it);
		std::vector<int> potentialVillageCenterCoords;

		std::vector<int> otherAgentFieldCoords;
		locationSpace->getLocation(otherAgentFieldLoc->getId(), otherAgentFieldCoords);

		potentialVillageCenterCoords.push_back((fieldCoords.at(0) + otherAgentFieldCoords.at(0))/2); // x coord
		potentialVillageCenterCoords.push_back((fieldCoords.at(1) + otherAgentFieldCoords.at(1))/2); // y coord

		// Check potential new village locations for village overlap
		std::vector<Location*> potentialVillageLocations;
		moore2DQuery.query(potentialVillageCenterCoords, param.villageRadius, false, potentialVillageLocations);

		bool isOverlap = false;
		for (std::vector<Location*>::iterator it2 = potentialVillageLocations.begin() ; it2 != potentialVillageLocations.end(); ++it2)
		{
			Location* potentialVillageLocation = (&**it2);
			if ((bool)potentialVillageLocation->getAssignedVillage())
			{
				isOverlap = true;
				break;
			}
		}

		if (isOverlap) { continue; } // if there if overlap, continue to next field location which matches criteria

		// If there isn't overlap, a village can be formed

		// find the household agent who's field triggered the village formation
		repast::SharedContext<Household>::const_iterator local_agents_iter = context.begin();
		repast::SharedContext<Household>::const_iterator local_agents_end = context.end();


		Household* otherHousehold = NULL;
		while(!otherHousehold && (local_agents_iter != local_agents_end))
		{
			Household* potentialOtherHousehold = (&**local_agents_iter);
			if (potentialOtherHousehold->getAssignedField()->getId().id() == otherAgentFieldLoc->getId().id()){
				otherHousehold = potentialOtherHousehold;
			}
			local_agents_iter++;
		}

		if (!otherHousehold) {
			return false;
		}

		std::vector<Household*> foundingHouseholds;

		foundingHouseholds.push_back(household);
		foundingHouseholds.push_back(otherHousehold);

		// Get village center location pointer
		std::vector<Location*> locationList;

		locationSpace->getObjectsAt(repast::Point<int>(potentialVillageCenterCoords[0], potentialVillageCenterCoords[1]), locationList);
		Location* newVillageCenter = locationList[0];

		int rank = repast::RepastProcess::instance()->rank();
		repast::AgentId id(villageID, rank, 3);

		// Create the new village and add it to context
		Village* newVillage = new Village(id, newVillageCenter, foundingHouseholds);

		villageContext.addAgent(newVillage);

		// Set household assigned village
		household->setAssignedVillage(newVillage);
		otherHousehold->setAssignedVillage(newVillage);

		// Set location assigned village
		for (std::vector<Location*>::iterator it2 = potentialVillageLocations.begin() ; it2 != potentialVillageLocations.end(); ++it2)
		{
			Location* potentialVillageLocation = (&**it2);
			potentialVillageLocation->setAssignedVillage(newVillage);
		}

		villageID++;

		return true;
	}
	return false;
}

void AnasaziModel::removeHousehold(Household* household)
{
	repast::AgentId id = household->getId();

	std::vector<int> loc;
	householdSpace->getLocation(id, loc);

	std::vector<Location*> locationList;
	std::vector<Household*> householdList;
	if(!loc.empty())
	{
		locationSpace->getObjectsAt(repast::Point<int>(loc[0], loc[1]), locationList);
		householdSpace->getObjectsAt(repast::Point<int>(loc[0], loc[1]), householdList);
		if(householdList.size() == 1)
		{
			locationList[0]->setState(0);
		}
		if(household->getAssignedField()!= NULL)
		{
			household->getAssignedField()->setState(0);
		}
	}
	if(household->getAssignedVillage()!=NULL)
	{
		Village* houseVillage = household->getAssignedVillage();
		houseVillage->removeHousehold(household->getId());
	}

	context.removeAgent(id);
}

bool AnasaziModel::relocateHousehold(Household* household, Village* needsVillageRemoved)
{
	std::vector<Location*> neighbouringLocations;
	std::vector<Location*> suitableLocations;
	std::vector<Location*> waterSources;
	std::vector<Location*> checkedLocations;

	Village* isVillager = household->getAssignedVillage();

	std::vector<int> loc, loc2;
	locationSpace->getLocation(household->getAssignedField()->getId(), loc);
	householdSpace->getLocation(household->getId(),loc2);

	locationSpace->getObjectsAt(repast::Point<int>(loc2[0], loc2[1]), neighbouringLocations);
	Location* householdLocation = neighbouringLocations[0];

	repast::Point<int> center(loc);
	repast::Moore2DGridQuery<Location> moore2DQuery(locationSpace);
	int range = floor(param.maxDistance/100);
	int i = 1;
	bool conditionC = true;

	//get all !Field with 1km
	LocationSearch:
		moore2DQuery.query(loc, range*i, false, neighbouringLocations);
		for (std::vector<Location*>::iterator it = neighbouringLocations.begin() ; it != neighbouringLocations.end(); ++it)
		{

			Location* tempLoc = (&**it); // need to check if this is a village

			bool isSameVillageLocation = isVillager && tempLoc->getAssignedVillage() ?
				(tempLoc->getAssignedVillage()->getId().id() == household->getAssignedVillage()->getId().id())
				: true;

			if (isSameVillageLocation) // check if the location is not part of a different village
			{
				if(tempLoc->getState() != 2)
				{
					if(householdLocation->getExpectedYield() < tempLoc->getExpectedYield() && conditionC == true)
					{
						suitableLocations.push_back(tempLoc);
					}
					if(tempLoc->getWater())
					{
						waterSources.push_back(tempLoc);
					}
				}
			}
		}
		if(suitableLocations.size() == 0 || waterSources.size() == 0)
		{
			if(conditionC == true)
			{
				conditionC = false;
			}
			else
			{
				conditionC = true;
				i++;
				if(range*i > boardSizeY)
				{
					if (needsVillageRemoved) {
						needsVillageRemoved->removeHousehold(household->getId());
					}	
					removeHousehold(household);

				
					return false;
				}
			}
			goto LocationSearch;
		}
		else if(suitableLocations.size() == 1)
		{
			std::vector<int> loc2;
			locationSpace->getLocation(suitableLocations[0]->getId(),loc2);
			householdSpace->moveTo(household->getId(),repast::Point<int>(loc2[0], loc2[1]));
			return true;
		}
		else
		{
			std::vector<int> point1, point2;
			std::vector<double> distances;
			for (std::vector<Location*>::iterator it1 = suitableLocations.begin() ; it1 != suitableLocations.end(); ++it1)
			{
				locationSpace->getLocation((&**it1)->getId(),point1);
				for (std::vector<Location*>::iterator it2 = waterSources.begin() ; it2 != waterSources.end(); ++it2)
				{
					locationSpace->getLocation((&**it2)->getId(),point2);
					double distance = sqrt(pow((point1[0]-point2[0]),2) + pow((point1[1]-point2[1]),2));
					distances.push_back(distance);
				}
			}
			int minElementIndex = std::min_element(distances.begin(),distances.end()) - distances.begin();
			minElementIndex = minElementIndex / waterSources.size();
			std::vector<int> loc2;
			locationSpace->getLocation(suitableLocations[minElementIndex]->getId(),loc2);
			householdSpace->moveTo(household->getId(),repast::Point<int>(loc2[0], loc2[1]));
			return true;
		}
}

// implementing proposed improvements:
void AnasaziModel::updateVillageProperties() {
	//get iterators to go over all villages: (find a solution for overlaping villages, ones that start at one process and extends to others)	

	repast::SharedContext<Village>::const_iterator local_agents_iter = villageContext.begin();
	repast::SharedContext<Village>::const_iterator local_agents_end = villageContext.end();

	while( local_agents_iter != local_agents_end ) 
	{
		Village* village = (&**local_agents_iter);
		
		std::vector<Household*> villageHouseholds = village -> getHouseholds();
		
		// Check if village should be removed 
		bool villageRemoved = (village -> getCurrentCapacity() < 2);

		if (!villageRemoved) {
			std::vector<Household*>::iterator iter;

			for (iter = villageHouseholds.begin(); iter != villageHouseholds.end(); ++iter)
			{
				if (!((*iter) -> getAssignedVillage()) || !((*iter)->getAssignedVillage()->getId().id() ==  village->getId().id()) || villageRemoved) {
					village->removeHousehold((*iter)->getId());

					if ((village -> getCurrentCapacity() < 2)) {		
						villageRemoved = true;
					}
				}
			}
		}

		if (villageRemoved) 
		{

			// remove class variables for household
			std::vector<Household*>::iterator iter2;
			for (iter2 = villageHouseholds.begin(); iter2 != villageHouseholds.end(); ++iter2)
			{	
				if ((*iter2) -> getAssignedVillage()) {
					if ((*iter2) -> getAssignedVillage()->getId().id() == village->getId().id()){	
						(*iter2) -> setAssignedVillage(NULL);
					}
				}
			}

			// remove class variables for location
			repast::SharedContext<Location>::const_iterator locations_iter = locationContext.begin();
			repast::SharedContext<Location>::const_iterator locations_end = locationContext.end();

			while( locations_iter != locations_end ) 
			{
				Location* loc = (&**locations_iter);
				Village* locVillage = loc -> getAssignedVillage();

				if (locVillage) {
					if (locVillage->getId().id() == village->getId().id()){
						loc->setAssignedVillage(NULL);
					}
				}

				locations_iter++;
			}
			local_agents_iter++;

			// remove the village
			villageContext.removeAgent(village->getId()); //Tom, have we added the code that removes a dying household from its corresponding village, if any?
		} else {
			village->redistributeMaize(param.householdNeed);

			local_agents_iter++;
		}
	} 
}